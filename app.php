<?php
/**
 * Application bootstrap
 *
 * Imports application application dependencies
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

// require the application configuration file
require_once(__DIR__ . '/config/config.php');

// require the application core
foreach (glob(CORE . '/*.php') as $filename) {
    require_once $filename;
}

// require the application models
foreach (glob(MODELS . '/*.php') as $filename) {
    require_once $filename;
}

// require the application controllers
foreach (glob(CONTROLLERS . '/*.php') as $filename) {
    require_once $filename;
}

// require the application routing configuration file
require_once(CONFIG . '/routes.php');
