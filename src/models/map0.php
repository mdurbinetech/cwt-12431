<?php
/**
 * Map class for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class MapOld
{
    private $mapFile;
    private $startPointId;
    private $endPointId;
    private $pathId;

    private $mapFileContents;
    private $mapDoc;

    private $viewBoxDefaultValue;

    /**
      * Constructor
      *
      * @param string mapFile
      * @param string startPointId
      * @param string endPointId
      * @param string pathId
      *
     **/
    public function __construct($mapFile = null, $startPointId = null, $endPointId = null, $pathId = null)
    {

         // error out if correct vars are not passed
        if ($mapFile == null) {
            // TODO handle error
            exit();
        } else {
            if ($startPointId == null or $endPointId == null or $pathId == null) {
                // TODO handle error
                exit();
            } else {
                if ($startPointId == 0 or $endPointId == 0 or $pathId == 0) {

                } else {
                    // doNothing();
                }
            }
        }

        // define object attributes from constructor parameters
        $this->mapFile = MAPS . '/' . $mapFile . '.svg';
        $this->startPointId = $startPointId;
        $this->endPointId = $endPointId;
        $this->pathId = $pathId;

        if (file_exists($this->mapFile)) {
            // get contents of map file
            $this->mapFileContents = file_get_contents($this->mapFile);
        } else {
            // TODO handle error
            exit();
        }

        // create DOMDocument
        $this->mapDoc = new DOMDocument();

        // load DOMDocument with text from file contents (string)
        $this->mapDoc->loadXML($this->mapFileContents);

        // establish xPath
        $this->xPath = new DOMXPath($this->mapDoc);

        // don't preserve whitespace
        $this->mapDoc->preserveWhiteSpace = false;

        // preserve default viewBox value
        $this->viewBoxDefaultValue = $this->getXmlAttribute('map', 'viewBox');

        // highlight startPoint
        $this->setXmlAttribute($startPointId, 'class', $this->getXmlAttribute($startPointId, 'class') . ' selected startPoint');

        // highlight endPoint
        $this->setXmlAttribute($endPointId, 'class', $this->getXmlAttribute($endPointId, 'class') . ' selected endPoint');

        // highlight path
        $this->setXmlAttribute($pathId, 'class', $this->getXmlAttribute($pathId, 'class') . ' selected path');

        // set the style
        // $this->setStyle(MAPS . '/css/map.css');
        $this->appendStyle(MAPS . '/css/map.css');

        // set map $bounds
        $this->setXmlAttribute('map', 'viewBox', $this->getViewBoxValues(
            $this->getElementBounds($this->startPointId),
            $this->getElementBounds($this->endPointId),
            $this->getPathBounds($this->pathId),
            100
         ));
    }

    /**
      * Gets xml attribute for given element
      *
      * @param string elemId
      * @param string attribute
      *
     **/
    public function getXmlAttribute($elemId, $attribute)
    {
        $elem = $this->xPath->query('//*[@id="'.$elemId.'"]')->item(0);
        if ($elem) {
            return $elem->getAttribute($attribute);
        } else {
            // TODO handle error
            exit();
        }
    }

    /**
      * Sets xml attribute for given element
      *
      * @param string elemId
      * @param string attribute
      * @param string value
      *
     **/
    public function setXmlAttribute($elemId, $attribute, $value)
    {
        $elem = $this->xPath->query('//*[@id="'.$elemId.'"]')->item(0);
        if ($elem) {
            $elem->setAttribute($attribute, $value);
        } else {
            // TODO handle error
        }
    }

    /**
      * Removes the style tag from the XML document
      *
     **/
    public function clearStyle()
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        if ($style) {
            $style->textContent = '';
        } else {
            // TODO handle error
        }
    }

    /**
      * Gets contents of the style tag from the XML document
      *
     **/
    public function getStyle()
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        return $style;
    }

    /**
      * Sets contents of the style tag from the XML document
      *
     **/
    public function setStyle($styleFile)
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        if ($style) {
            $style->textContent = file_get_contents($styleFile);
        } else {
            // TODO handle error
        }
    }

    public function appendStyle($styleFile) {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        if ($style) {
            $style->textContent = $style->textContent . file_get_contents($styleFile);
        } else {
            // TODO handle error
        }
    }

    /**
      * Saves and renders the map file to screen
      *
     **/
    public function render()
    {
        try {
            echo $this->mapDoc->saveXML();
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
      * Gets coordinates of rectangular element bounds (top-left and bottom-right)
      *
      * @param string elemId
      *
     **/
    public function getElementBounds($elemId)
    {
        $bounds = array();
        $elem = $this->xPath->query('//*[@id="'.$elemId.'"]')->item(0);
        if ($elem) {
            $boundElem = $this->xPath->query('//*[@id="_'.$elemId.'"]')->item(0);
            if($boundElem){
                $x1 = $this->getXmlAttribute('_'.$elemId, 'x');
                $y1 = $this->getXmlAttribute('_'.$elemId, 'y');
                $x2 = $x1 + $this->getXmlAttribute('_'.$elemId, 'width');
                $y2 = $y1 + $this->getXmlAttribute('_'.$elemId, 'height');

                $bounds['x1'] = $x1;
                $bounds['y1'] = $y1;
                $bounds['x2'] = $x2;
                $bounds['y2'] = $y2;



            } else {
                // TODO handle error
                echo 'err';
            }

        } else {
            // TODO handle error

        }
        return $bounds;
    }

    /**
      * Gets coordinates of element bounds (top-left and bottom-right)
      *
      * @param string elemId
      *
     **/
    public function getPathBounds($pathId) {
        $path = $this->xPath->query('//*[@id="'.$pathId.'"]')->item(0);
        if ($path) {
            $points = $this->getXmlAttribute($pathId, 'points');
            if(strlen($points)>0) {
                $pointsArray = explode(' ', $points);
                $xPointsArray= [];
                $yPointsArray=[];
                foreach($pointsArray as $idx => $point) {
                    if($idx % 2 == 0) {
                        array_push($xPointsArray, $point);
                    } else {
                        array_push($yPointsArray, $point);
                    }
                }
                $lowX = min($xPointsArray);
                $lowY = min($yPointsArray);

                $maxX = min($xPointsArray);
                $maxY = min($yPointsArray);

                $bounds = [];

                $bounds['x1'] = $lowX;
                $bounds['y1'] = $lowY;
                $bounds['x2'] = $maxX;
                $bounds['y2'] = $maxY;

                return $bounds;
            } else {
                return $this->getElementBounds($this->startPointId);
            }


        } else {
            // TODO handle error
            // echo 'err';
            return $this->getElementBounds($this->startPointId);
        }








    }

    /**
      * Gets coordinates of element bounds (top-left and bottom-right)
      *
      * @param array $elem1Bounds
      * @param array $elem2Bounds
      * @param array $elem3Bounds
      *
     **/
    public function getViewBoxValues($elem1Bounds = null, $elem2Bounds = null, $elem3Bounds = null, $padding = 100)
    {
        if ($elem1Bounds == null || $elem2Bounds == null || $elem3Bounds == null) {
            // return $this->viewBoxDefaultValue;
            $viewBoxDefaultValueArray = explode(' ', $this->viewBoxDefaultValue);
            $viewBoxDefaultValueArray[0] = $viewBoxDefaultValueArray[0] - $padding;
            $viewBoxDefaultValueArray[1] = $viewBoxDefaultValueArray[1] - $padding;
            $viewBoxDefaultValueArray[2] = $viewBoxDefaultValueArray[2] + 2*$padding;
            $viewBoxDefaultValueArray[3] = $viewBoxDefaultValueArray[3] + 2*$padding;

            return implode(' ', $viewBoxDefaultValueArray);
        } else {
            $xRay = array($elem1Bounds['x1'], $elem1Bounds['x2'], $elem2Bounds['x1'], $elem2Bounds['x2'], $elem3Bounds['x1'], $elem3Bounds['x2']);
            $yRay = array($elem1Bounds['y1'], $elem1Bounds['y2'], $elem2Bounds['y1'], $elem2Bounds['y2'], $elem3Bounds['y1'], $elem3Bounds['y2']);

            $x = min($xRay) - $padding;
            $y = min($yRay) - $padding;
            $w = max($xRay) - $x + $padding;
            $h = max($yRay) - $y + $padding;

            return $x . ' ' . $y . ' ' . $w . ' ' . $h;
        }
    }
}
