<?php
/**
 * Map class for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

require_once( RESOURCES . '/SvgBounds/SvgBounds.php');

class Map
{
    private $mapFile;
    private $startPointId;
    private $endPointId;
    private $pathId;

    private $mapFileContents;
    private $mapDoc;
    private $xPath;

    private $viewBoxDefaultValue;

    private $startPointElem;
    private $endPointElem;
    private $startPointBoundsElem;
    private $endPointBoundsElem;
    private $pathElem;

    private $mapElem;

    /**
      * Constructor
      *
      * @param string mapFile
      * @param string startPointId
      * @param string endPointId
      * @param string pathId
      *
     **/
    public function __construct($mapFile = null, $startPointId = null, $endPointId = null, $pathId = null)
    {

        // echo $pathId;

        // determine if all vars have been set
        if ($mapFile == null || $startPointId == null || $endPointId == null || $pathId == null) {
        } else {

            // determine if there is an svg file for this map name
            if (file_exists(MAPS . '/' . $mapFile . '.svg')) {
                $this->mapFile = MAPS . '/' . $mapFile . '.svg';

                // create the map document
                $this->mapFileContents = str_replace('
', '', file_get_contents($this->mapFile));

// echo $this->mapFileContents;

// exit();

// exit();



                // create DOMDocument
                $this->mapDoc = new DOMDocument();

                // load DOMDocument with text from file contents (string)
                $this->mapDoc->loadXML($this->mapFileContents);

                // establish xPath
                $this->xPath = new DOMXPath($this->mapDoc);

                $this->mapElem = $this->getElementById('map');

                // print_r($this->mapElem);
                // exit();

                $this->setXmlAttribute($this->mapElem, 'class', $mapFile);

                // preserve default viewBox value
                $this->viewBoxDefaultValue = $this->getXmlAttribute($this->mapElem, 'viewBox');

                // print_r($this->viewBoxDefaultValue);
                // exit();


                if ($startPointId == "0" or $endPointId == "0" or $pathId == "0") {

                    // echo 'here';
                    // exit();

                    // just show the map if no vars are defined

                } else {

                    // print_r($this->getElementById($startPointId));
                    // print_r($this->getElementById('_'.$startPointId));
                    // print_r($this->getElementById($endPointId));
                    // print_r($this->getElementById('_'.$endPointId));
                    // print_r($this->getElementById($pathId));

                    // echo '(' .$endPointId . ')';

                    // exit();

                    // if all the elements exist on map, continue building map;
                    if (
                        $this->getElementById($startPointId) != null and
                        $this->getElementById('_'.$startPointId) != null and
                        $this->getElementById($endPointId) != null and
                        $this->getElementById('_'.$endPointId) != null and
                        $this->getElementById($pathId) != null
                    ) {

                        // echo 'here';
                        // exit();

                        $this->startPointElem = $this->getElementById($startPointId);
                        $this->endPointElem = $this->getElementById($endPointId);
                        $this->startPointBoundsElem = $this->getElementById('_'.$startPointId);
                        $this->endPointBoundsElem = $this->getElementById('_'.$endPointId);
                        $this->pathElem = $this->getElementById($pathId);

                        // print_r($pathId);
                        // exit();

                        // $this->getElementBounds($this->startPointElem);
                        // $this->getElementBounds($this->endPointElem);
                        // $this->getElementBounds($this->startPointBoundsElem);
                        // $this->getElementBounds($this->endPointBoundsElem);
                        // $this->getElementBounds($this->pathElem);

                        // set map $bounds
                        $this->setXmlAttribute($this->mapElem, 'viewBox', $this->getViewBoxValues(
                            $this->getElementBounds($this->startPointBoundsElem),
                            $this->getElementBounds($this->endPointBoundsElem),
                            $this->getElementBounds($this->pathElem),
                            100
                         ));

                        // highlight startPoint
                        $this->setXmlAttribute($this->startPointElem, 'class', $this->getXmlAttribute($this->startPointElem, 'class') . ' selected startPoint');

                        // highlight endPoint
                        $this->setXmlAttribute($this->endPointElem, 'class', $this->getXmlAttribute($this->endPointElem, 'class') . ' selected endPoint');

                        // highlight path
                        $this->setXmlAttribute($this->pathElem, 'class', $this->getXmlAttribute($this->pathElem, 'class') . ' selected path');


                    } else {

                        // else just send the map w/o bounds and stuff

                    }
                }
            } else {

                // doNothing();
            }
        }

        $this->setStyle($this->reReferenceStyle($this->getStyle(), '.'.$mapFile.''));

        $this->appendStyle(MAPS . '/css/map.css');
    }

    /**
      * Gets xml attribute for given element
      *
      * @param string elemId
      * @param string attribute
      *
     **/
    public function getXmlAttribute($elem, $attribute)
    {
        if ($elem != null) {
            return $elem->getAttribute($attribute);
        } else {
            // TODO handle error
            return null;
        }
    }

    /**
      * Sets xml attribute for given element
      *
      * @param string elemId
      * @param string attribute
      * @param string value
      *
     **/
    public function setXmlAttribute($elem, $attribute, $value)
    {
        if ($elem !=null) {
            return $elem->setAttribute($attribute, $value);
        } else {
            // TODO handle error
            return false;
        }
    }

    /**
      * Removes the style tag from the XML document
      *
     **/
    public function clearStyle()
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        if ($style) {
            $style->textContent = '';
        } else {
            // TODO handle error
        }
    }

    /**
      * Gets contents of the style tag from the XML document
      *
     **/
    public function getStyle()
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        return $style->textContent;
    }

    /**
      * Sets contents of the style tag from the XML document
      *
     **/
    public function setStyle($styleFileContents)
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        if ($style) {
            $style->textContent = $styleFileContents;
        } else {
            // TODO handle error
        }
    }

    public function reReferenceStyle($styleSheetText, $newReference) {
        return str_replace('.cls-', $newReference.' .cls-',$styleSheetText);
    }

    public function appendStyle($styleFile)
    {
        $style = $this->mapDoc->getElementsByTagName('style')->item(0);
        if ($style) {
            $style->textContent = $style->textContent . ' ' . file_get_contents($styleFile);
        } else {
            // TODO handle error
        }
    }

    /**
      * Saves and renders the map file to screen
      *
     **/
    public function render()
    {
        try {
            // echo $this->mapDoc->saveXML();
            $map = $this->mapDoc->saveXML();
            $map =  str_replace('<?xml version="1.0"?>', '', $map = str_replace('xmlns="http://www.w3.org/2000/svg"', 'xmlns="http://www.w3.org/2000/svg" version="1.1"', $map));
            echo $map;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
      * Gets coordinates of rectangular element bounds (top-left and bottom-right)
      *
      * @param string elemId
      *
     **/
    public function getElementBounds($elem)
    {

        // print_r($elem);

        // create the bounds array
        $bounds = [];

        // determine what type of element it is (line, polyline, rect, etc)
        $elemType = $elem->tagName;

        // calculate bounds based on elemType
        switch ($elemType) {
            case "polygon":
                // coords attribute(s) format: points="x y x y . . ."

                $pointsArray = explode(' ', $this->getXmlAttribute($elem, 'points'));
                $xPointsArray= [];
                $yPointsArray=[];
                foreach ($pointsArray as $idx => $point) {
                    if ($idx % 2 == 0) {
                        array_push($xPointsArray, $point);
                    } else {
                        array_push($yPointsArray, $point);
                    }
                }
                $lowX = min($xPointsArray);
                $lowY = min($yPointsArray);

                $maxX = min($xPointsArray);
                $maxY = min($yPointsArray);

                $bounds = [];

                $bounds['x1'] = (int) $lowX;
                $bounds['y1'] = (int) $lowY;
                $bounds['x2'] = (int) $maxX;
                $bounds['y2'] = (int) $maxY;

                return $bounds;

                break;
            case "rect":
                // coords attribute(s) format: x=,y=,width=,height=

                // echo '('. round($this->getXmlAttribute($elem, 'height')) . ')';
                // exit();

                $x1 = $this->getXmlAttribute($elem, 'x');
                $y1 = $this->getXmlAttribute($elem, 'y');
                $x2 = $x1 + $this->getXmlAttribute($elem, 'width');
                $y2 = (int) $y1 + $this->getXmlAttribute($elem, 'height');

                // set the bounds
                $bounds['x1'] = (int) $x1;
                $bounds['y1'] = (int) $y1;
                $bounds['x2'] = (int) $x2;
                $bounds['y2'] = (int) $y2;

                return $bounds;

                break;
            case "polyline":
                // coords attribute(s) format: points="x y x y . . ."
                $pointsArray = explode(' ', $this->getXmlAttribute($elem, 'points'));
                $xPointsArray= [];
                $yPointsArray=[];
                foreach ($pointsArray as $idx => $point) {
                    if ($idx % 2 == 0) {
                        array_push($xPointsArray, $point);
                    } else {
                        array_push($yPointsArray, $point);
                    }
                }
                $lowX = min($xPointsArray);
                $lowY = min($yPointsArray);

                $maxX = max($xPointsArray);
                $maxY = max($yPointsArray);

                $bounds = [];

                $bounds['x1'] = (int) $lowX;
                $bounds['y1'] = (int) $lowY;
                $bounds['x2'] = (int) $maxX;
                $bounds['y2'] = (int) $maxY;

                // print_r($xPointsArray);
                // print_r($yPointsArray);

                return $bounds;

                break;
            case "line":
                // coords attribute(s) format: x1=,y1=,x2=,y2=
                $x1 = $this->getXmlAttribute($elem, 'x1');
                $y1 = $this->getXmlAttribute($elem, 'y1');
                $x2 = $this->getXmlAttribute($elem, 'x2');
                $y2 = $this->getXmlAttribute($elem, 'y2');

                // set the bounds
                $bounds['x1'] = (int) $x1;
                $bounds['y1'] = (int) $y1;
                $bounds['x2'] = (int) $x2;
                $bounds['y2'] = (int) $y2;

                return $bounds;

                break;
            case "path":



                // $d = $this->getXmlAttribute($elem, 'd');
                // print_r(preg_replace("/[^0-9,.]/", " ", $d));
                //
                //
                // print_r(preg_replace("/[^0-9,.]/", " ", $d));
                //
                //
                //
                // exit();

                return 0;

                break;
        }



    }


    /**
      * Gets coordinates of element bounds (top-left and bottom-right)
      *
      * @param array $elem1Bounds
      * @param array $elem2Bounds
      * @param array $elem3Bounds
      *
     **/
    public function getViewBoxValues($elem1Bounds = null, $elem2Bounds = null, $elem3Bounds = null, $padding = 100)
    {

        // print_r($elem1Bounds);
        // print_r($elem2Bounds);
        // print_r($elem3Bounds);

        if ($elem1Bounds == null || $elem2Bounds == null ) {
            $viewBoxDefaultValueArray = explode(' ', $this->viewBoxDefaultValue);
            $viewBoxDefaultValueArray[0] = $viewBoxDefaultValueArray[0] - $padding;
            $viewBoxDefaultValueArray[1] = $viewBoxDefaultValueArray[1] - $padding;
            $viewBoxDefaultValueArray[2] = $viewBoxDefaultValueArray[2] + 2*$padding;
            $viewBoxDefaultValueArray[3] = $viewBoxDefaultValueArray[3] + 2*$padding;

            return implode(' ', $viewBoxDefaultValueArray);
        } else {
            $xRay = array($elem1Bounds['x1'], $elem1Bounds['x2'], $elem2Bounds['x1'], $elem2Bounds['x2'], $elem3Bounds['x1'], $elem3Bounds['x2']);
            $yRay = array($elem1Bounds['y1'], $elem1Bounds['y2'], $elem2Bounds['y1'], $elem2Bounds['y2'], $elem3Bounds['y1'], $elem3Bounds['y2']);

            $x = min($xRay) - $padding;
            $y = min($yRay) - $padding;
            $w = max($xRay) - $x + $padding;
            $h = max($yRay) - $y + $padding;

            return $x . ' ' . $y . ' ' . $w . ' ' . $h;
        }
    }

    public function getElementById($elemId)
    {
        $elem = $this->xPath->query('//*[@id="'.$elemId.'"]')->item(0);

        if ($elem != null) {
            return $elem;
        } else {
            return null;
        }
    }
}
