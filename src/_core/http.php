<?php
/**
 * Http class for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class Http
{
    protected $ch;

    public function __construct()
    {
        // create curl resource
        $this->ch = curl_init();

        //return the transfer as a string
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }

    public function get($url)
    {
        // set url
        curl_setopt($this->ch, CURLOPT_URL, $url);

        // $output contains the output string
        $output = curl_exec($this->ch);

        return $output;
    }

    public function __destruct()
    {
        // close curl resource to free up system resources
        curl_close($this->ch);
    }
}
