<?php
/**
 * Application database class
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class Database
{
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    private $dbh;
    private $error;

    private $stmt;

    public function __construct()
    {
        // Set DSN
        $dsn = 'odbc:=' . $this->host . ';dbname=' . $this->dbname;
        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );
        // Create a new PDO instanace
        try {
            $this->dbh = new PDO('odbc:Driver={SQL Server};Server='.DB_HOST.';Database='.DB_NAME.'; Uid='.DB_USER.';Pwd='.DB_PASS.';');
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log($this->error);
        }
    }

    /**
      * Query the database
      *
      * @param string $query
     **/
    public function query($query)
    {
        // Prepare the statement
        try {
            $this->stmt = $this->dbh->prepare($query);
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log($this->error);
        }
    }

    /**
      * Bind parameter in prepared statement
      *
      * @param string $param
      * @param string $value
      * @param string $type
     **/
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                $type = PDO::PARAM_INT;
                break;
                case is_bool($value):
                $type = PDO::PARAM_BOOL;
                break;
                case is_null($value):
                $type = PDO::PARAM_NULL;
                break;
                default:
                $type = PDO::PARAM_STR;
            }
        }
        // Bind parameter
        try {
            $this->stmt->bindValue($param, $value, $type);
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log($this->error);
        }
    }

    /**
      * Execute statement
      *
     **/
    public function execute()
    {
        // Execute the statement
        try {
            return $this->stmt->execute();
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log($this->error);
        }
    }

    /**
      * Return multiple results from executed statement
      *
     **/
    public function resultset()
    {
        // Execute the statement
        try {
            $this->execute();
            return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log($this->error);
        }
    }

    /**
      * Return a single result from executed statement
      *
     **/
    public function single()
    {
        // Execute the statement
        try {
            $this->execute();
            return $this->stmt->fetch(PDO::FETCH_ASSOC);
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log($this->error);
        }
    }

    /**
      * Return number of results from statement execution
      *
     **/
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }

    /**
      * Return the ID of the last record inserted
      *
     **/
    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }

    /**
      * Begin database transaction
      *
     **/
    public function beginTransaction()
    {
        return $this->dbh->beginTransaction();
    }

    /**
      * End database transaction
      *
     **/
    public function endTransaction()
    {
        return $this->dbh->commit();
    }

    /**
      * Debug dumped parameters from statement
      *
     **/
    public function debugDumpParams()
    {
        return $this->stmt->debugDumpParams();
    }
}
