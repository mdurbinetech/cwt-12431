<?php
/**
 * Base class for all view classes
 *
 * Renders HTML
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class View
{
    protected $layout = 'default';
    protected $folder;
    protected $file;
    protected $variables;

    public function __construct()
    {
        $this->variables = array();
        $this->setVariable('pageTitle', DEFAULT_PAGE_TITLE);
    }

    /**
      * Set view to render
      *
      * @param string $folder
      * @param string $file
     **/
    public function set($folder, $file)
    {
        $this->folder = $folder;
        $this->file = $file;
    }

    /**
      * Set layout for view
      *
      * @param string $layout
     **/
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
      * Set variables in the view file
      *
      * @param string $key
      * @param string $value
     **/
    public function setVariable($key, $value)
    {
        $this->variables[$key] = $value;
    }

    /**
      * Renders the view
      *
     **/
    public function render()
    {
        // extract variables so that they can be read by the view file
        extract($this->variables);

        // begin output buffer
        ob_start();

        // set viewFilename
        $viewFilename = VIEWS . '/' . $this->folder . '/' . $this->file . '.php';

        // if file exists, include it
        if (file_exists($viewFilename)) {
            include($viewFilename);
        } else {
            // TODO error message?
            // doNothing();
        }

        // clean output buffer
        $layoutContent = ob_get_clean();

        // set layoutFilename
        $layoutFilename = LAYOUTS . '/' . $this->layout . '.php';

        // if file exists, include it
        if (file_exists($layoutFilename)) {
            include($layoutFilename);
        } else {
            // TODO error message?
            // doNothing();
        }
    }
}
