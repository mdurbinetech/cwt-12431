<?php
/**
 * Base class for API controllers
 *
 * Sets up basic CRUD functions for use in API controllers.
 * Enables JSON output.
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class ApiController
{
    private $dboName;
    private $model;

    public function __construct($dboName = null)
    {
        $this->dboName = $dboName;
    }

    /**
      * Renders the API root view
      *
     **/
    public function index()
    {
        $this->json([
            'message'=>'This is the API root.'
        ]);
    }

    /**
      *  Execute query to locate all rows from a database object
      *
      * @param array $fields
      * @param array $constraints
      * @param array $order
     **/
    public function findAll($fields = array(), $constraints = array(), $order = array())
    {
        $this->model = new Model($this->dboName);
        $data = $this->model->findAll($fields, $constraints, $order);
        $this->json($data);
    }

    /**
      *  Execute query to locate a single row from a database object
      *
      * @param int $id
      * @param array $fields
      * @param array $constraints
      * @param array $order
     **/
    public function find($id, $fields = array(), $constraints = array(), $order = array())
    {
        $this->model = new Model($this->dboName);
        $data = $this->model->findOne($id, $fields, $constraints, $order);
        $this->json($data);
    }

    /**
      * Output data in array as a JSON object
      *
      * @param array $data
     **/
    public function json($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }

}
