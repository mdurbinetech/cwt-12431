<?php
/**
 * Base class for all model classes
 *
 * Creates simple CRUD and view methods
 * Logic present is intended to handle Tables only as it has full CRUD logic
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class Model extends Dbo
{
    public function insert()
    {
    }

    public function update()
    {
    }

    public function delete()
    {
    }
}
