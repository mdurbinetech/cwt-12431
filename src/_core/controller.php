<?php
/**
 * Base class for all controller classes
 *
 * Binds models to views
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class Controller
{
    protected $view;
    protected $controllerName = '';
    protected $http;

    public function __construct()
    {
        $this->http = new Http();
        $this->view = new View();
        $this->setControllerName();
    }

    /**
      * Generates view for index page of controller's path
      *
     **/
    public function index()
    {
        $this->setView($this->controllerName, 'index');
    }

    /**
      * Generates view for 404 errors
      *
     **/
    public function notFound()
    {
        $this->setView($this->controllerName, '404');
    }

    /**
      * Generates view for 403 errors
      *
     **/
    public function unauthorized()
    {
        $this->setView($this->controllerName, '403');
    }

    /**
      * Set controller name to be used
      *
     **/
    protected function setControllerName()
    {
        if (get_called_class() == 'Controller') {
            // doNothing();
        } else {
            $this->controllerName = strtolower(str_replace('Controller', '', get_called_class()));
        }
    }

    /**
      * Set view file to be used
      *
      * @param string $controllerName
      * @param string $action
     **/
    protected function setView($controllerName, $action)
    {
        $this->view->set($controllerName, $action);
    }

    /**
      * Set variable to be used in view
      *
      * @param string $key
      * @param string $value
     **/
    public function setViewVariable($key, $value)
    {
        $this->view->setVariable($key, $value);
    }

    /**
      * Set page title for view
      *
      * @param string $pageTitle
     **/
    protected function setViewPageTitle($pageTitle)
    {
        $this->setVariable('pageTitle', $title);
    }

    public function setViewLayout($layout) {
        $this->view->setLayout($layout);
    }

    public function __destruct()
    {
        $this->view->render();
    }
}
