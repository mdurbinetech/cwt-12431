<?php
/**
 * Application router class
 *
 * Extends AltoRouter class
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

require_once RESOURCES . '/AltoRouter/AltoRouter.php';

class Router extends AltoRouter
{
    /**
      * Function wrapper to send GET requests.
      * See AltoRouter class for further variable explanation
      *
      * @param string $route
      * @param mixed $target
      * @param string $name
     **/
    public function get($route, $target, $name = null)
    {
        $this->map('GET', $route, $target, $name);
    }

    /**
      * Function wrapper to send POST requests.
      * See AltoRouter class for further variable explanation
      *
      * @param string $route
      * @param mixed $target
      * @param string $name
     **/
    public function post($route, $target, $name = null)
    {
        $this->map('POST', $route, $target, $name);
    }

    /**
      * Function wrapper to send PUT requests.
      * See AltoRouter class for further variable explanation
      *
      * @param string $route
      * @param mixed $target
      * @param string $name
     **/
    public function put($route, $target, $name = null)
    {
        $this->map('PUT', $route, $target, $name);
    }

    /**
      * Function wrapper to send PATCH requests.
      * See AltoRouter class for further variable explanation
      *
      * @param string $route
      * @param mixed $target
      * @param string $name
     **/
    public function patch($route, $target, $name = null)
    {
        $this->map('PATCH', $route, $target, $name);
    }

    /**
      * Function wrapper to send DELETE requests.
      * See AltoRouter class for further variable explanation
      *
      * @param string $route
      * @param mixed $target
      * @param string $name
     **/
    public function delete($route, $target, $name = null)
    {
        $this->map('DELETE', $route, $target, $name);
    }
}
