<?php
/**
 * Base class for all database objects
 *
 * Creates simple CRUD and view methods
 * Logic present is intended to handle basic functions for all database objects.
 * Methods find(), findAll(), and query()--no CRUD functions.
 * For CRUD functions, use the Model class
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class Dbo
{
    private $db = null;
    private $dbo = null;

    public function __construct($dboName = null)
    {
        if ($dboName == null) {
            error_log('No dbo was specified. Unable to continue with database request.');
            exit();
        } else {
            $this->db = new Database();
            $this->setDbo($dboName);
        }
    }

    public function setDbo($dbo)
    {
        $this->dbo = $dbo;
    }

    /**
      *  Execute query to locate a single row from a database object
      *
      * @param int $id
      * @param array $fields
      * @param array $constraints
      * @param array $order
     **/
    public function find($id, $fields = array(), $constraints = array(), $order = array())
    {
        $statement = "
            SELECT
        ";

        // Fields to select from dbo
        if (count($fields) > 0) {
            $statement .= join(', ', $fields);
        } else {
            $statement .= " * ";
        }

        // FROM clause
        if ($this->dbo == null) {
            //TODO handle error
            exit();
        } else {
            $statement .= ' FROM ' . $this->dbo;
        }

        // WHERE clause
        if (count($constraints) > 0) {
            $statement .= ' WHERE '.rtrim($this->dbo, 's').'ID = ' . $id . join(' AND ', $constraints);
        } else {
            $statement .= ' WHERE '.rtrim($this->dbo, 's').'ID = ' . $id;
        }

        // ORDER BY clause
        if (count($order) > 0) {
            $statement .= ' ORDER BY ' . join(', ', $order);
        } else {
            //doNothing();
        }

        // add a semi-colon to the end of the statement
        $statement .= ';';

        // prepare statement
        $this->db->query($statement);

        // execute statement
        $this->db->execute();

        // retrieve result
        $data = $this->db->single();

        // returns result
        return $data;
    }

    /**
      *  Execute query to locate all rows from a database object
      *
      * @param array $fields
      * @param array $constraints
      * @param array $order
     **/
    public function findAll($fields = array(), $constraints = array(), $order = array())
    {
        $statement = "
            SELECT
        ";

        // Fields to select from dbo
        if (count($fields) > 0) {
            $statement .= join(', ', $fields);
        } else {
            $statement .= " * ";
        }

        // FROM clause
        if ($this->dbo == null) {
            //TODO handle error
            exit();
        } else {
            $statement .= ' FROM ' . $this->dbo;
        }

        // WHERE clause
        if (count($constraints) > 0) {
            $statement .= ' WHERE ' . join(' AND ', $constraints);
        } else {
            // doNothing();;
        }

        // ORDER BY clause
        if (count($order) > 0) {
            $statement .= ' ORDER BY ' . join(', ', $order);
        } else {
            // doNothing();
        }

        // add a semi-colon to the end of the statement
        $statement .= ';';

        // prepare statement
        $this->db->query($statement);

        // execute statement
        $this->db->execute();

        // retrieve results
        $data = $this->db->resultSet();

        // returns result
        return $data;
    }


    /**
      *  Execute a custom query on set dbo
      *
      * @param string $query
     **/
    public function query($query)
    {
        // prepare statement
        $this->db->query($query);

        // execute statement
        $this->db->execute();

        // retrieve results
        $data = $this->db->resultSet();

        return $data;
    }
}
