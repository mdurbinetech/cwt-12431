<html>
    <head>
        <title>
            <?php echo $pageTitle; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
        <!-- CSS -->
        <link href="/assets/css/directions.css" rel="stylesheet">
    </head>

    <body>
        <?php echo $layoutContent; ?>
        <!-- JS -->
        <script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="/assets/js/popper.min.js"></script>
        <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    </body>
</html>
