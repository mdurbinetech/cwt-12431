<html>
    <head>
        <title>
            <?php echo $pageTitle; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <!-- CSS -->
        <link href="/assets/css/jkeyboard.css" rel="stylesheet">
        <link href="/assets/css/style.css" rel="stylesheet">
    </head>

    <body>
        <?php echo $layoutContent; ?>
        <!-- JS -->
        <script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="/assets/js/popper.min.js"></script>
        <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/jkeyboard.js"></script>
        <script type="text/javascript" src="/assets/js/tool.js"></script>
    </body>
</html>
