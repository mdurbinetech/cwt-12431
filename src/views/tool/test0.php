<?php
    if (isset($_GET["startPointId"])) {
        // doNothing();
    } else {
        echo 'ERROR: <strong>startPointId</strong> not specified';
        exit();
    }
?>

<div id="page_header">
    <button id="backButton" data-targetPanel="init" class="">
        Back
    </button>
</div>
<!-- / #page_header -->


<div id="page_content">
    <ul class="nav nav-tabs" id="tabNav" role="tablist" class="d-none">
        <li class="nav-item">
            <a class="nav-link active" id="init-tab" data-toggle="tab" href="#init" role="tab" aria-controls="init" aria-selected="true">Init</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="sessionSearch-tab" data-toggle="tab" href="#sessionSearch" role="tab" aria-controls="sessionSearch" aria-selected="false">Session Search</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="sessionSearchResults-tab" data-toggle="tab" href="#sessionSearchResults" role="tab" aria-controls="sessionSearchResults" aria-selected="false">Session Search Results</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="venues-tab" data-toggle="tab" href="#venues" role="tab" aria-controls="venues" aria-selected="false">Venues</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="venueMenu-tab" data-toggle="tab" href="#venueMenu" role="tab" aria-controls="venueMenu" aria-selected="false">Venue Menu</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="rooms-tab" data-toggle="tab" href="#rooms" role="tab" aria-controls="rooms" aria-selected="false">Rooms</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="transportationOptions-tab" data-toggle="tab" href="#transportationOptions" role="tab" aria-controls="transportationOptions" aria-selected="false">Transportation Options</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="routeSteps-tab" data-toggle="tab" href="#routeSteps" role="tab" aria-controls="routeSteps" aria-selected="false">Route Steps</a>
        </li>
    </ul>
    <div class="tab-content" id="tabNavContent" data-currentPanel="init">
        <div class="tab-pane fade show active" id="init" role="tabpanel" aria-labelledby="init-tab"></div>
        <!-- / #init -->
        <div class="tab-pane fade" id="sessionSearch" role="tabpanel" aria-labelledby="sessionSearch-tab"></div>
        <!-- / #sessionSearch -->
        <div class="tab-pane fade" id="sessionSearchResults" role="tabpanel" aria-labelledby="sessionSearchResults-tab"></div>
        <!-- / #sessionSearchResults -->
        <div class="tab-pane fade" id="venues" role="tabpanel" aria-labelledby="venues-tab"></div>
        <!-- / #venues -->
        <div class="tab-pane fade" id="venueMenu" role="tabpanel" aria-labelledby="venueMenu-tab"></div>
        <!-- venueMenu -->
        <div class="tab-pane fade" id="rooms" role="tabpanel" aria-labelledby="rooms-tab"></div>
        <!-- / #rooms -->
        <div class="tab-pane fade" id="transportationOptions" role="tabpanel" aria-labelledby="transportationOptions-tab"></div>
        <!-- / #transportationOptions -->
        <div class="tab-pane fade" id="routeSteps" role="tabpanel" aria-labelledby="routeSteps-tab"></div>
        <!-- / #routeSteps -->
    </div>

</div>
<!-- / #page_content -->

<div id="page_footer">

</div>
<!-- / #page_footer -->
