<div class="panel-header">
    <h1 class="panel-heading">
        <strong><?php echo $venue; ?></strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
            <ul id="venue_menu_list">
                <?php
                    foreach ($venueMenuItems as $venueMenuItem) {
                        // print_r($venueMenuItem);
                        // exit();
                        if ($venueMenuItem['NAME'] == "Specific Room") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-rooms" data-venueId="'.trim($venueId).'" data-buttonName="venueMenu: '.$venueMenuItem['NAME'].'">';
                            echo '<span><strong>'.$venueMenuItem['NAME'].'</strong></span>';
                            echo '</button></li>';
                        } elseif ($venueMenuItem['NAME'] == "Meals") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-pick-venues" data-endOption="Meals" data-venueId="'.trim($venueId).'" data-buttonName="venueMenu: '.$venueMenuItem['NAME'].'">';
                            echo '<span><strong>'.$venueMenuItem['NAME'].'</strong></span>';
                            echo '</button></li>';
                        } else {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$venueMenuItem['POINTID'].'" data-venueId="'.$venueId.'" data-buttonName="venueMenu: '.$venueMenuItem['NAME'].'">';
                            echo '<span><strong>'.$venueMenuItem['NAME'].'</strong></span>';
                            echo '</button></li>';
                        }
                    }
                ?>
            </ul>
            <!-- / .content-row -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
