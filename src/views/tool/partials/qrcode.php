<?php

    include(RESOURCES.'/phpqrcode-master/qrlib.php');

    // outputs image directly into browser, as PNG stream
    // QRcode::png($data, '007_1.png', QR_ECLEVEL_L, 15, 0);
    // echo '<img src="'.'/007_1.png" class="img-fluid mb-5 w-50" />';
    QRcode::png($data);
