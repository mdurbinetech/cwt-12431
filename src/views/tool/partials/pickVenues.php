<div class="panel-header">
    <h1 class="panel-heading">
        <strong><?php echo $pickVenues['HEADING'];?></strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
            <ul id="venue_menu_list">
                <?php
                $pickVenuesLocations = $pickVenues['LOCATIONS'];
                // print_r($pickVenues);
                // exit();
                    foreach ($pickVenuesLocations as $pickVenuesMenuItem) {
                        if ($pickVenues['HEADING'] == "Room") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-rooms" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'" data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['PRETTYNAME'].'</strong></span>';
                            echo '</button></li>';
                        } elseif ($pickVenues['HEADING'] == "Meals") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$pickVenuesMenuItem['ENDPOINTID'].'" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'" data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['VENUENAME'].'</strong><br>'.$pickVenuesMenuItem['PRETTYNAME'].'</span>';
                            echo '</button></li>';
                        } elseif ($pickVenues['HEADING'] == "Registration") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$pickVenuesMenuItem['ENDPOINTID'].'" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'" data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['VENUENAME'].'</strong><br>'.$pickVenuesMenuItem['PRETTYNAME'].'</span>';
                            echo '</button></li>';
                        } elseif ($pickVenues['HEADING'] == "Shuttle") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$pickVenuesMenuItem['ENDPOINTID'].'" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'" data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['VENUENAME'].'</strong><br>'.$pickVenuesMenuItem['PRETTYNAME'].'</span>';
                            echo '</button></li>';
                        } elseif ($pickVenues['HEADING'] == "Content Hub") {
                            // echo 'test';
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$pickVenuesMenuItem['ENDPOINTID'].'" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'" data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['VENUENAME'].'</strong><br>'.$pickVenuesMenuItem['PRETTYNAME'].'</span>';
                            echo '</button></li>';
                        } elseif ($pickVenues['HEADING'] == "Help") {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$pickVenuesMenuItem['ENDPOINTID'].'" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'"  data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['VENUENAME'].'</strong><br>'.$pickVenuesMenuItem['PRETTYNAME'].'</span>';
                            echo '</button></li>';
                        } else {
                            echo '<li class="col col-6"><button class="venue-menu-button loads-route-steps"  data-endPointId="'.$pickVenuesMenuItem['ENDPOINTID'].'" data-venueId="'.$pickVenuesMenuItem['VENUEID'].'" data-buttonName="pickVenues: '.$pickVenuesMenuItem['PRETTYNAME'].'">';
                            echo '<span><strong>'.$pickVenuesMenuItem['VENUENAME'].'</strong><br>'.$pickVenuesMenuItem['PRETTYNAME'].'</span>';
                            echo '</button></li>';
                        }
                    }
                ?>
            </ul>
            <!-- / .content-row -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
