<?php
    $http_maps = new Http();
?>


<div class="panel-header">
    <h1 class="panel-heading">
        <small><?php echo $routeInfo['STARTVENUEROOM']; ?></small>
        <strong><?php echo $routeInfo['ENDVENUEROOM']; ?></strong>
    </h1>
</div>
<div class="touchable-area h-75">
    <div id="routeStepsCarousel" class="carousel slide h-75" data-interval="false">
    	<div class="carousel-inner h-100">
            <?php $stepCount = count($routeSteps); ?>
            <?php foreach($routeSteps as $index => $routeStep): ?>
                <?php
                    // print_r($routeStep);
                ?>
                <?php if($index == 0): ?>
                <div class="carousel-item active">
                <?php else: ?>
                <div class="carousel-item">
                <?php endif; ?>
                    <div class="content-container h-100">
                                <div class="map-container">
                    <?php



                    $routeStartId=explode('-', $routeStep['ROUTECSSNAME'])[0];
                    $routeEndId=explode('-', $routeStep['ROUTECSSNAME'])[1];

                    // echo 'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId;
                    // echo $routeStep['ROUTECSSNAME'];

                    ?>

                    <?php

                    if($routeStep['MAPLINK'] != "ELEV" &&  $routeStep['MAPLINK'] != "ESC") {
                        echo $http_maps->get('http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId);
                        // // echo '<img src="'.'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId.'">';
                        // echo '<object class="w-100 h-100" style="overflow: hidden" data="'.'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId.'" type="image/svg+xml"></object>';

                    } else {
                        echo '<img style="max-width: 70%; max-height: 70%;" src="/assets/img/'.$routeStep['MAPLINK'].'.png">';
                    }

                    ?>
                                </div>
                                <!-- / .map-container -->
                                <div class="route-steps-container h-50">
                                    <p class="step-info">
                                        Step <?php echo $routeStep['STEP']; ?> of <?php echo $stepCount; ?>
                                    </p>
                                    <p class="step-text">
                                        <?php echo $routeStep['ROUTETEXT']; ?>
                                    </p>
                                </div>
                                <!-- / .route-steps-container -->
                    </div>
                    <!-- / .content-container -->
                </div>
                <!-- / .carousel-item -->
            <?php endforeach; ?>
            <a class="carousel-control-prev" href="#routeStepsCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#routeStepsCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
    	</div>
        <!-- / .carousel-inner -->
        <img src="<?php echo $routeQrCodeUrl ?>" class="route-qr-code">
    </div>
    <!-- / #routeStepsCarousel -->

</div>
<!-- / .touchable-area -->
<div class="non-touchable-area h-0 text-center">

</div>
<!-- / .non-touchable-area -->
