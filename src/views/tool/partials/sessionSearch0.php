<?php
    if (isset($errorText)) {
        echo '<p>' . $errorText . '</p>';
    } else {
        // doNothing();
    }
?>
<div id="container_sessionSearch" class="content-container">
    <div class="content-row">
        <div id="content_sessionSearch">
            <div class="content-row">
                <div class="content-touchable-range">
                    <div class="content-row">
                        <div id="controls_sessionSearch">
                            <div class="input-group" id="input_group_sessionSearchField">
                                <input id="sessionSearchField" type="text" class="form-control" placeholder="Search Terms" aria-label="Search Terms" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                <button class="btn btn-outline-secondary loads-panel loads-sessionSearchResults" type="button" id="button-addon2" data-targetPanel="sessionSearchResults">Search</button>
                                </div>
                            </div>
                            <!-- / #input_group_sessionSearchField -->
                            <p id="help_text_sessionSearchField">
                                For example, try search for a session ID, name room, or speaker
                            </p>
                            <div id="sessionSearchKeyboard"></div>
                            <script>
                                $('#sessionSearchKeyboard').jkeyboard({
                                  layout: "english",
                                  input: $('#sessionSearchField')
                                });
                            </script>
                        </div>
                        <!-- / #controls_sessionSearch -->
                    </div>
                    <!-- / .content-row -->
                </div>
                <!-- / .content-touchable-range -->
                <div class="content-non-touchable-range">

                </div>
                <!-- / .content-touchable-range -->
            </div>
            <!-- / .content-row -->
        </div>
        <!-- / #content_init -->
    </div>
    <!-- / .content-row -->
</div>
<!-- / #container_sessionSearch -->
