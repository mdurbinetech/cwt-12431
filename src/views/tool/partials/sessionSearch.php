<div class="panel-header">
    <h1 class="panel-heading">
        Search <strong>Sessions</strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
        <div class="content-row">
            <div id="session_search_controls">
                <div class="mb-4 form">
                    <span id="clear_button">Clear</span>
                    <input id="session_search_field" type="search" class="form-control border-white" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                    <button class="d-flex align-items-center justify-content-center px-3 btn-theme loads-session_search_results" type="button" id="button-addon2" data-targetPanel="sessionSearchResults">
                        <span class="sr-only">Search</span>
                    </button>
                </div>
                <!-- / #input_group_sessionSearchField -->
                <p class="help-text mb-4 text-center">
                    Search for a session ID, session title, or room.
                </p>
                <div id="session_search_keyboard"></div>
                <script>
                    $('#session_search_keyboard').jkeyboard({
                      layout: "english",
                      input: $('#session_search_field')
                    });
                </script>
            </div>
            <!-- / #session_search_controls -->
        </div>
        <!-- / .content-row -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
