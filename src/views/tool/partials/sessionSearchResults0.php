<div class="touchable-area">
    <div class="content-container">
        <div class="panel-header">
            <h1 class="panel-heading">
                Search <strong>Sessions</strong>
            </h1>
        </div>
        <ul id="venues_list">
            <?php

                foreach ($venues as $venue) {
                    echo '<li>';
                    echo '<button class="venue-button" data-venueId="'.$venue['VENUEID'].'">';
                    echo '<span>'.$venue['VENUENAME'].'</span>';
                    echo '</button>';
                    echo '</li>';
                }
            ?>
        </ul>
        <!-- / #venues_list -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->





<?php

    foreach ($sessionSearchResults as $index => $sessionSearchResult) {
        if ($index == 0) {
            ?>
            <h2>Top Result</h2>
            <ul>
                <li><?php echo $sessionSearchResult['SESSION']; ?></li>
                <li><?php echo $sessionSearchResult['STARTTIME']; ?></li>
                <li><?php echo $sessionSearchResult['ROOM']; ?></li>
                <li>
                    <button class="loads-panel loads-routeSteps" data-targetPanel="routeSteps" data-endPointId="<?php echo $sessionSearchResult['ENDPOINTID']; ?>" data-endVenueId="<?php echo $sessionSearchResult['ENDVENUEID']; ?>">
                        Go
                    </button>
                </li>
            </ul>
        <?php
        } else {
            // doNothing();
        }
    }
?>

<h2>
    More Results (#)
</h2>
<ul>

<?php

    foreach ($sessionSearchResults as $index => $sessionSearchResult) {
        if ($index == 0) {
            // doNothing();
        } else {
            ?><li><ul>

                <li><?php echo $sessionSearchResult['SESSION']; ?></li>
                <li><?php echo $sessionSearchResult['STARTTIME']; ?></li>
                <li><?php echo $sessionSearchResult['ROOM']; ?></li>
                <li>
                    <button class="loads-panel loads-transportationOptions" data-targetPanel="transportationOptions" data-endPointId="<?php echo $sessionSearchResult['ENDPOINTID']; ?>" data-endVenueId="<?php echo $sessionSearchResult['ENDVENUEID']; ?>">
                        Go
                    </button>
                </li>
            </ul></li>
        <?php
        }
    }
?>

<ul>
