<!-- <div class="panel-header">
    <h1 class="panel-heading">
        Search <strong>Sessions</strong>
    </h1>
</div> -->
<div class="touchable-area">
    <div class="content-container scrollable-vertical">
        <div class="content-row align-items-center">
            <div class="w-100">
                <div id="session_search_controls" class="mb-5">
                    <div class="mb-4 form">
                        <span id="clear_button">Clear</span>
                        <input id="session_search_field" type="search" class="form-control border-white" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                        <button class="d-flex align-items-center justify-content-center px-3 btn-theme loads-session_search_results" type="button" id="button-addon2" data-targetPanel="sessionSearchResults">
                            <span class="sr-only">Search</span>
                        </button>
                    </div>
                    <!-- / #input_group_sessionSearchField -->
                    <p class="help-text mb-4 text-center">
                        Search for a session ID, session title, or room.
                    </p>
                    <button class="btn btn-primary d-none" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Button with data-target
                    </button>
                    <div class="collapse text-center" id="collapseExample">
                        <div id="session_search_keyboard"></div>
                        <script>
                            $('#session_search_keyboard').jkeyboard({
                              layout: "english",
                              input: $('#session_search_field')
                            });
                        </script>
                        <button id="collapse_keyboard" style="transform: scale(0.35)" class="btn btn-danger mt-4">Close Keyboard</button>
                    </div>
                </div>
                <!-- / #session_search_controls -->

                <div id="whereDoYouWantToGoCollapse" class="collapse show w-100">
                    <h1 class="h3 mb-3" style="font-weight: 800">
                        Where do you<br>want to go?
                    </h1>
                    <div class="w-100">
                        <ul id="whereDoYouWantToGoList" class="list-unstyled row">
                            <?php
                            $venuesArray = $whereDoYouWantToGo['VENUES'];

                            $contentRoomsArray = $whereDoYouWantToGo['CONTENTROOMS'];

                            $conferenceServicesArray = $whereDoYouWantToGo['CONFERENCESERVICES'];

                            $allArrays = array_merge($venuesArray, $contentRoomsArray, $conferenceServicesArray);

                            // "If EndpPointID = '', then call API_PickVenues.cfm to get the venue where they want to go. If IsNumeric(EndPointID), then call TransportationMethods.cfm, EXCEPT if EndOption = 'Rideshare', then call RouteSteps.cfm",

                            foreach ($allArrays as $key => $value) {
                                echo '<li class="col col-4 mb-3">';
                                // handle venues first
                                if (isset($allArrays[$key]['VENUENAME'])) {
                                    $buttonHtml = '<button
                                                    class="venue-button"
                                                    data-venueId="'.$allArrays[$key]['VENUEID'].'"
                                                    >
                                                        '.$allArrays[$key]['VENUENAME'].'
                                                    </button>
                                    ';
                                    echo $buttonHtml;
                                } elseif ($allArrays[$key]['ENDOPTION'] == 'Rideshare') {
                                    // handle rideshare separately
                                    $buttonHtml = '<button
                                                    class="loads-route-steps"
                                                    data-venueId="0"
                                                    data-endPointId="'.$allArrays[$key]['ENDPOINTID'].'"
                                                    >
                                                        '.$allArrays[$key]['LOCATION'].'
                                                    </button>
                                    ';
                                    echo $buttonHtml;
                                } elseif ($allArrays[$key]['ENDPOINTID'] != '' && $allArrays[$key]['VENUEID'] != '') {
                                    // handle rideshare separately
                                    $buttonHtml = '<button
                                                    class="loads-route-steps"
                                                    data-venueId="'.$allArrays[$key]['VENUEID'].'"
                                                    data-endPointId="'.$allArrays[$key]['ENDPOINTID'].'"
                                                    >
                                                        '.$allArrays[$key]['LOCATION'].'
                                                    </button>
                                    ';
                                    echo $buttonHtml;
                                } else {
                                    // handle others
                                    $buttonHtml = '<button
                                                        class="loads-pick-venues"

                                                        data-endOption="'.$allArrays[$key]['ENDOPTION'].'"
                                                        >
                                                            '.$allArrays[$key]['LOCATION'].'
                                                        </button>
                                        ';
                                    echo $buttonHtml;
                                }
                            }


                            ?>

                        </ul>
                        <!-- / .row -->

                    </div>
                    <!-- / .col -->
                </div>
                <!-- / #whereDoYouWantToGoCollapse -->
            </div>
        </div>
        <!-- / .content-row -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
