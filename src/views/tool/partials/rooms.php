<div class="panel-header">
    <h1 class="panel-heading">
        <?php echo $venueName; ?><br><strong>Rooms</strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
            <?php if (isset($venueRooms) && count($venueRooms) > 0) : ?>
                <ul id="rooms_list">
                    <?php foreach ($venueRooms as $index => $venueRoom): ?>
                        <li>
                            <?php echo $venueRoom['PRETTYNAME']; ?>
                            <button class="room-select-button" data-endPointId="<?php echo $venueRoom['ENDPOINTID']; ?>" data-venueId="<?php echo $venueId; ?>" data-buttonName="room: <?php echo $venueRoom['PRETTYNAME']; ?>"><span class="sr-only"><?php echo $venueRoom['PRETTYNAME']; ?></span></button>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <!-- / #session_search_results -->
            <?php endif; ?>
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
