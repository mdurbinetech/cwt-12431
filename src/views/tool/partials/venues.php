<div class="panel-header">
    <h1 class="panel-heading">
        <strong>Venues</strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
        <ul id="venues_list">
            <?php

                foreach ($venues as $venue) {
                    echo '<li class="col col-6">';
                    echo '<button class="venue-button" data-venueId="'.$venue['VENUEID'].'" data-buttonName="venues: '.$venue['VENUENAME'].'">';
                    echo '<span><strong>'.$venue['VENUENAME'].'</span></strong>';
                    echo '</button>';
                    echo '</li>';
                }
            ?>
        </ul>
        <!-- / #venues_list -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
