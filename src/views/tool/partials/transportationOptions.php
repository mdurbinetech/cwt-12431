<?php

    $req_url = $_SERVER['REQUEST_URI'];
    $req_url_array = array_reverse(explode('/', $req_url));

    $endPointId = $req_url_array[1];
    $startPointId = $req_url_array[2];

    // if there are no options returned from the API, then option is walk
    if (count($transportationOptions['METHODS']) < 1) {
        // skip ahead to routeSteps
    } else {
        ?>

<div class="panel-header">
    <h1 class="panel-heading">
        <small><?php echo $transportationOptions['STARTVENUEROOM']; ?></small>
        <strong><?php echo $transportationOptions['ENDVENUEROOM']; ?></strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
            <ul id="transportation_options_list">
                <?php

                    $shuttleInArray = false;

        foreach ($transportationOptions['METHODS'] as $transportationOption): ?>

                    <?php
                        if (in_array('Shuttle', $transportationOption)) {
                            $shuttleInArray = true;
                        } ?>

                            <li class="col col-4">
                                <button
                                    class="transportation-option-select-button"
                                    data-startPointId="<?php echo $startPointId; ?>"
                                    data-endPointId="<?php echo $endPointId; ?>"
                                    data-venueId="<?php echo $transportationOptions['ENDVENUEID']; ?>"
                                    data-buttonName="transportatonOptions: <?php echo $transportationOption['METHOD']; ?>"
                                    data-transportationMode="<?php echo $transportationOption['METHOD']; ?>"><span><span class="icon <?php echo strtolower($transportationOption['METHOD']); ?>"></span><strong>
                                    <?php echo ucwords(strtolower($transportationOption['METHOD'])); ?></strong></span></button>
                            </li>
                <?php endforeach; ?>

                <?php
                    if ($shuttleInArray) {
                        // doNothing();
                    } else {
                        ?>
                        <li class="col col-4">
                            <button class="disabled-transpo-button" disabled>
                                <span>
                                    <span class="icon shuttle"></span>
                                    <small><strong class="text-orange">No shuttle offered<br>for this route</strong></small>
                                </span>
                            </button>
                        </li>
                        <?php
                    } ?>
            </ul>
            <!-- / .content-row -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->


<?php
    }
