<?php
    $http_maps = new Http();
?>


<div class="panel-header">
    <h1 class="panel-heading">
        <small><?php echo $routeInfo['STARTVENUEROOM']; ?></small>
        <strong><?php echo $routeInfo['ENDVENUEROOM']; ?></strong>
    </h1>
</div>
<div class="touchable-area h-75">
    <div id="routeStepsCarousel" class="carousel slide h-75" data-interval="false">
    	<div class="carousel-inner h-100">
            <?php $stepCount = count($routeSteps); ?>
            <?php
                // print_r($routeSteps);
                // exit();
            ?>
            <?php foreach ($routeSteps as $index => $routeStep): ?>
                <?php
                    // print_r($routeStep);

                ?>
                <script>
                    console.log('<?php print_r(json_encode($routeStep, true)) ?>');
                </script>
                <?php if ($index == 0): ?>
                <div class="carousel-item active">
                <?php else: ?>
                <div class="carousel-item">
                <?php endif; ?>
                    <div class="content-container h-100">
                                <div class="map-container">


                    <?php

                    // echo $routeStep['MAPLINK'] . '<br>' .$routeStep['ROUTECSSNAME'];

                    $routeStartId=explode('-', $routeStep['ROUTECSSNAME'])[0];
                    $routeEndId=explode('-', $routeStep['ROUTECSSNAME'])[1];

                    // echo 'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId;
                    // echo $routeStep['ROUTECSSNAME'];

                    ?>

                    <?php

                    if ($routeStep['MAPLINK'] != "ELEV" &&  $routeStep['MAPLINK'] != "ESC") {
                        echo $http_maps->get(PROTOCOL.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId);
                        // // echo '<img src="'.'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId.'">';
                        // echo '<object class="w-100 h-100" style="overflow: hidden" data="'.'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId.'" type="image/svg+xml"></object>';?>
                            <script>
                                endPointElem = document.getElementById("<?php echo 'ARI_'.$routeStep['ENDPOINTID']; ?>");
                                console.log(endPointElem);

                            </script>
                        <?php
                    } else {
                        echo '<img style="max-width: 70%; max-height: 70%;" src="/assets/img/'.$routeStep['MAPLINK'].'.png">';
                    }

                    ?>
                                </div>
                                <!-- / .map-container -->
                                <div class="route-steps-container h-50">
                                    <button id="qr_code_button" class="btn btn-sm btn-block mt-5 mb-3" data-toggle="modal" data-target="#myOtherOtherOtherModal" data-buttonName="routeSteps: Show QR Code"><i class="fas fa-lg fa-qrcode text-orange" ></i>&nbsp; <strong>Scan &amp; take directions with you</strong></button>
                                    <p class="step-text">
                                        <?php echo $routeStep['ROUTETEXT']; ?>
                                    </p>
                                    <p class="step-info">
                                        Step <?php echo $routeStep['STEP']; ?> of <?php echo $stepCount; ?>
                                    </p>
                                </div>
                                <!-- / .route-steps-container -->

                                <?php
                                if ($index == count($routeSteps) - 1) {
                                    ?>
                                    <script>
                                        console.log('last step: <?php echo $routeEndId; ?>');
                                        finalEndPoint = $('#<?php echo $routeEndId; ?>');
                                        $(finalEndPoint).addClass('final-endPoint');
                                        // $('#_<?php echo $routeEndId; ?>').addClass('final-endPoint');
                                    </script>
                                <?php
                                }
                                ?>


                    </div>
                    <!-- / .content-container -->
                </div>
                <!-- / .carousel-item -->
            <?php endforeach; ?>

            <div class="modal fade" id="myOtherOtherOtherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body px-5 pb-5 text-center">

                        <?php
                            echo '<img src="'.PROTOCOL.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'].'/tool/test/qrcode/'.$routeQrCodeUrl.'" class="img-fluid mb-5 w-50" />';
                        ?>

                        <p class="text-orange text-center">
                            Scan the code<br>with your mobile device<br>to take these directions with you.
                        </p>
                     </div>
                  </div>
               </div>
            </div>


            <a class="carousel-control-prev" href="#routeStepsCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#routeStepsCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
    	</div>
        <!-- / .carousel-inner -->
        <!-- <img src="<?php echo $routeQrCodeUrl ?>" class="route-qr-code"> -->
    </div>
    <!-- / #routeStepsCarousel -->

</div>
<!-- / .touchable-area -->
<div class="non-touchable-area h-0 text-center">

</div>
<!-- / .non-touchable-area -->

<script>

$('#routeStepsCarousel').on('slid', '', checkitem);  // on caroussel move
$('#routeStepsCarousel').on('slid.bs.carousel', '', checkitem); // on carousel move

$(document).ready(function(){               // on document ready
    checkitem();
});

function checkitem()                        // check function
{
    // var $this = $('#routeStepsCarousel');
    if($('.carousel-item').length < 2) {
        $('.carousel-control-prev').hide();
        $('.carousel-control-next').hide();
        console.log('test');
    } else {

        if($('.carousel-item:first').hasClass('active')) {
            $('.carousel-control-prev').hide();
            $('.carousel-control-next').show();
        } else if($('.carousel-item:last').hasClass('active')) {
            $('.carousel-control-prev').show();
            $('.carousel-control-next').hide();
        } else {
            $('.carousel-control-prev').show();
            $('.carousel-control-next').show();
        }
    }
}

</script>
