<!-- <div class="panel-header">
    <h1 class="panel-heading">
        Search <strong>Sessions</strong>
    </h1>
</div> -->
<div class="touchable-area">
    <div class="content-container scrollable-vertical">
        <div class="content-row align-items-center">
            <div class="w-100">
                <div id="session_search_controls" class="mb-5">
                    <div class="mb-4 form">
                        <span id="clear_button">Clear</span>
                        <input id="session_search_field" type="search" class="form-control border-white" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                        <button class="d-flex align-items-center justify-content-center px-3 btn-theme loads-session_search_results" type="button" id="button-addon2" data-targetPanel="sessionSearchResults">
                            <span class="sr-only">Search</span>
                        </button>
                    </div>
                    <!-- / #input_group_sessionSearchField -->
                    <p class="help-text mb-4 text-center">
                        Search for a session ID, session title, or room.
                    </p>
                    <button class="btn btn-primary d-none" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Button with data-target
                    </button>
                    <div class="collapse text-center" id="collapseExample">
                        <div id="session_search_keyboard"></div>
                        <script>
                            $('#session_search_keyboard').jkeyboard({
                              layout: "english",
                              input: $('#session_search_field')
                            });
                        </script>
                        <button id="collapse_keyboard" style="transform: scale(0.35)" class="btn btn-danger mt-4">Close Keyboard</button>
                    </div>
                </div>
                <!-- / #session_search_controls -->

                <div id="whereDoYouWantToGoCollapse" class="collapse show w-100">
                    <h1 class="h3 mb-3" style="font-weight: 800">
                        Where do you<br>want to go?
                    </h1>
                    <div class="w-100">
                        <ul id="whereDoYouWantToGoList" class="list-unstyled row">




                        <?php

                            // echo json_encode($whereDoYouWantToGo);
                            // exit();

                            // foreach ($whereDoYouWantToGo['VENUES'] as $venue) {
                            //     echo '
                            //     <li class="col col-4 mb-3">
                            //         <button class="venue-button" data-venueId="'.$venue['VENUEID'].'">'.$venue['VENUENAME'].'</button>
                            //     </li>
                            //     ';
                            // }
                        ?>

                        <?php

                            // foreach ($whereDoYouWantToGo['CONTENTROOMS'] as $contentroom) {

                                // If EndpPointID = '', then call API_PickVenues.cfm to get the venue where they want to go. If IsNumeric(EndPointID), then call TransportationMethods.cfm, EXCEPT if EndOption = 'Rideshare', then call RouteSteps.cfm

                                // $buttonHtmlOpeningTag = '<button>test';
                                //
                                // if ($contentroom['ENDPOINTID'] = '') {
                                //     // create a button that calls API_PickVenues.cfm
                                //     $buttonHtmlOpeningTag = '<button>test2';
                                // } else {
                                //     // if ($contentroom['ENDOPTION'] == 'Rideshare') {
                                //     // create a button that calls Routesteps.cfm
                                //     $buttonHtmlOpeningTag = '<button class="loads-route-steps" data-transportationMode="walk" data-endPointid="'.$contentroom['ENDPOINTID'].'">test3';
                                //     // } else {
                                //     // create a button that calls TransportationMethods.cfm
                                //     $buttonHtmlOpeningTag = '<button>test4';
                                //     // }
                                // }
                                //
                                // echo '
                                // <li class="col col-4 mb-3">
                                //     '.$buttonHtmlOpeningTag.$contentroom['LOCATION'].'</button>
                                // </li>
                                // ';
                            // }
                        ?>

                        <?php

                        // print_r($whereDoYouWantToGo['CONFERENCESERVICES']);

                        $venues = $whereDoYouWantToGo['VENUES'];

                        foreach ($venues as $key => $value) {
                            $buttonHtml = '
                                <li class="col-4 mb-3">
                                    <button
                                        class="venue-button" data-venueId="'.$venues[$key]['VENUEID'].'">
                                        '.$venues[$key]['VENUENAME'].'
                                    </button>
                                </li>
                            ';
                            echo $buttonHtml;
                        }

                        $conferenceServices = $whereDoYouWantToGo['CONFERENCESERVICES'];

                        // print_r($conferenceServices);

                        foreach ($conferenceServices as $key => $value) {
                            if ($conferenceServices[$key]['ENDPOINTID'] == '') {
                                // echo $conferenceServices[$key]['ENDPOINTID'];
                                $buttonHtml = '
                                    <li class="col-4 mb-3">
                                        <button
                                            class="loads-pick-venues"
                                            data-endOption="'.$conferenceServices[$key]['ENDOPTION'].'">
                                            '.$conferenceServices[$key]['LOCATION'].'
                                        </button>
                                    </li>
                                ';
                                echo $buttonHtml;
                            } else {
                                if ($conferenceServices[$key]['ENDOPTION'] == 'Rideshare') {
                                    // echo $conferenceServices[$key]['ENDPOINTID'];
                                    $buttonHtml = '
                                        <li class="col-4 mb-3">
                                            <button
                                                class="loads-route-steps"
                                                data-endPointId="'.$conferenceServices[$key]['ENDPOINTID'].'"
                                                data-transportationMode="walk"
                                                data-venueId="0">
                                                '.$conferenceServices[$key]['LOCATION'].'
                                            </button>
                                        </li>
                                    ';
                                    echo $buttonHtml;
                                } else {
                                    $buttonHtml = '
                                        <li class="col-4 mb-3">
                                            <button
                                                class="loads-route-steps"
                                                data-endPointId="'.$conferenceServices[$key]['ENDPOINTID'].'"
                                                data-transportationMode="walk"
                                                data-venueId="0">
                                                '.$conferenceServices[$key]['LOCATION'].'
                                            </button>
                                        </li>
                                    ';
                                    echo $buttonHtml;
                                }
                            }
                        }

                            // foreach ($conferenceservices as $conferenceservice) {
                                // If EndpPointID = '', then call API_PickVenues.cfm to get the venue where they want to go. If IsNumeric(EndPointID), then call TransportationMethods.cfm, EXCEPT if EndOption = 'Rideshare', then call RouteSteps.cfm

                                // print_r($conferenceservice);

                                // $confsvc =

                            //     $buttonHtmlOpeningTag = '<button>test';
                            //
                            //     if ($conferenceservice['ENDPOINTID'] = '') {
                            //         // create a button that calls API_PickVenues.cfm
                            //         $buttonHtmlOpeningTag = '<button>test2';
                            //     } else {
                            //         if ($conferenceservice['LOCATION'] == 'Rideshare') {
                            //             print_r($conferenceservice);
                            //
                            //             // print_r($conference)
                            //             // create a button that calls Routesteps.cfm
                            //             $buttonHtmlOpeningTag = '<button class="loads-route-steps" data-transportationmode="walk" data-endpointid="'.$conferenceservice['ENDPOINTID'].'" data-venueid="0">test3';
                            //         } else {
                            //             // create a button that calls TransportationMethods.cfm
                            //             $buttonHtmlOpeningTag = '<button>test4';
                            //         }
                            //     }
                            //
                            //     echo '
                            //     <li class="col col-4 mb-3">
                            //         '.$buttonHtmlOpeningTag.$conferenceservice['LOCATION'].'</button>
                            //     </li>
                            //     ';
                            // }


                                                            // unset($conferenceservice);

                            // print_r($whereDoYouWantToGo);


                        ?>
                        </ul>
                        <!-- / .row -->

                    </div>
                    <!-- / .col -->
                </div>
                <!-- / #whereDoYouWantToGoCollapse -->
            </div>
        </div>
        <!-- / .content-row -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
