<?php
    $http_maps = new Http();
?>

<div id="page_wrapper">

    <div>

        <div id="directions_header">

            <div class="container">

                <h1 class="directions-heading">
                    <small><?php echo $routeInfo['STARTVENUEROOM']; ?></small>
                    <strong><?php echo $routeInfo['ENDVENUEROOM']; ?></strong>
                </h1>
                <!-- / .directions-heading -->

            </div>
            <!-- / .container -->

        </div>
        <!-- / .directions-header -->

        <div id="directions_carousel" class="carousel slide" data-interval="false" data-touch="false">

        	<div class="carousel-inner">
            <?php $stepCount = count($routeSteps); ?>
            <?php foreach ($routeSteps as $index => $routeStep): ?>

                <div class="carousel-item<?php if ($index == 0) {
    echo ' active';
} ?>">

                    <div class="map-container">

                        <?php

                        $routeStartId=explode('-', $routeStep['ROUTECSSNAME'])[0];
                        $routeEndId=explode('-', $routeStep['ROUTECSSNAME'])[1];

                        if ($routeStep['MAPLINK'] != "ELEV" &&  $routeStep['MAPLINK'] != "ESC") {
                            echo $http_maps->get(PROTOCOL.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] .'/api/map/'.$routeStep['MAPLINK'].'/'.$routeStartId.'/'.$routeEndId);
                        } else {
                            echo '<img style="max-width: 70%; max-height: 70%;" src="/assets/img/'.$routeStep['MAPLINK'].'.png">';
                        }

                        ?>

                    </div>
                    <!-- / .map-container -->

                    <div class="info-container container">

                        <div class="spacer"> </div>

                        <div class="step-text">
                            <p>
                                Step <?php echo $routeStep['STEP']; ?> of <?php echo $stepCount; ?>
                            </p>
                        </div>
                        <!-- / .step-text -->

                        <p class="step-detail">
                            <?php echo $routeStep['ROUTETEXT']; ?>
                        </p>

                    </div>
                    <!-- / .info -->

                </div>
                <!-- / .carousel-item -->
            <?php endforeach; ?>

                <div class="controls-container">

                    <a class="carousel-control-prev" href="#directions_carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <!-- / .carousel-control-next -->

                    <a class="carousel-control-next" href="#directions_carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!-- / .carousel-control-next -->

                </div>
                <!-- / .controls-container -->

        	</div>
            <!-- / .carousel-inner -->

        </div>
        <!-- / #directions_carousel -->

    </div>
    <!-- / anon div -->

</div>
<!-- / #page_wrapper -->


<script>
    $('#routeStepsCarousel').carousel('pause');
    $('#routeStepsCarousel').carousel({"touch": false});
</script>
