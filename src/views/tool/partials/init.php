<div class="panel-header">
</div>
<div class="touchable-area">
    <div class="content-container">
        <div id="init_buttons">
            <button id="session_search_button" class="loads-session_search"><span>Search <strong>Sessions</strong></span></button>
            <button id="venues_button" class="loads-venues"><span><strong>Find Venues</strong><br>and Other Resources</span></button>
            <!-- <button id="places_button" class="loads-places">Places</button> -->
        </div>
        <!-- / #init_buttons -->
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
