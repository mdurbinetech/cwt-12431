<?php
    $numResults = count($sessionSearchResults);
?>

<div class="panel-header">
    <h1 class="panel-heading">
        Search <strong>Results</strong>
    </h1>
</div>
<div class="touchable-area">
    <div class="content-container">
        <p><em>Your search for "<?php echo $searchTerms; ?>" returned (<?php echo $numResults; ?>) results.</em></p>
        <?php if (isset($sessionSearchResults) && $numResults > 0) : ?>
            <ul id="session_search_results_list">
                <?php foreach ($sessionSearchResults as $index => $sessionSearchResult): ?>
                    <script>
                        // console.log('<?php echo $sessionSearchResult['SESSION']; ?>');
                    </script>
                    <li>
                        <ul>
                            <li class="session-title"><?php echo $sessionSearchResult['SESSION']; ?></li>
                            <li class="session-start-time"><small><strong><?php echo $sessionSearchResult['TIMESTR']; ?></strong></small></li>
                            <li class="session-room"><small><strong><?php echo $sessionSearchResult['ROOM']; ?></strong></small></li>
                        </ul>
                        <button class="session-select-button" data-endPointId="<?php echo $sessionSearchResult['ENDPOINTID']; ?>" data-venueId="<?php echo $sessionSearchResult['ENDVENUEID']; ?>" data-buttonName='session: <?php echo $sessionSearchResult['SESSION']; ?>'></button>
                    </li>
                <?php endforeach; ?>
            </ul>
            <!-- / #session_search_results -->
        <?php else: ?>
            <p class="mb-5 mt-5">Sorry, there were no results that matched your search terms.  Try searching again by session ID, session title, or room.  You may also ask a re:Invent Ambassador for further assistance.</p>
        <?php endif; ?>
        <button class="retry-search btn-theme btn-block back-button"><strong>Search Again</strong></button>
        </div>
    </div>
    <!-- / .content-container -->
</div>
<!-- / .touchable-area -->
<div class="non-touchable-area">
</div>
<!-- / .non-touchable-area -->
