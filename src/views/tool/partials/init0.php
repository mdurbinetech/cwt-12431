<div id="container_init" class="content-container">
    <div class="content-row">
        <div id="content_init">
            <div class="content-row">
                <div class="content-touchable-range">
                    <div class="content-row">
                        <div id="buttons_init">
                            <button id="sessionSearchButton" data-targetPanel="sessionSearch" class="loads-sessionSearch loads-panel clicks-tracked" data-clickTrackName="Init Menu Button - Sessions">
                                Search <strong>Sessions</strong>
                            </button>
                            <!-- / #sessionSearchButton -->

                            <button id="venuesButton" data-targetPanel="venues" class="loads-venues loads-panel clicks-tracked" data-clickTrackName="Init Menu Button - Venues">
                                <strong>Find Venues</strong><br>
                                and Other Resources
                            </button>
                            <!-- / #venuesButton -->

                        </div>
                        <!-- / #buttons_init -->
                    </div>
                    <!-- / .content-row -->
                </div>
                <!-- / .content-touchable-range -->
                <div class="content-non-touchable-range">

                </div>
                <!-- / .content-touchable-range -->
            </div>
            <!-- / .content-row -->
        </div>
        <!-- / #content_init -->
    </div>
    <!-- / .content-row -->
</div>
<!-- / #container_init -->
