<?php
    if (isset($_GET["startPointId"])) {
        // doNothing();
    } else {
        echo 'ERROR: <strong>startPointId</strong> not specified';
        exit();
    }
?>

<div id="page_wrapper">

    <div id="page_header">
        <div class="content-container">
            <div class="content-row">
                <div id="page_navigation">
                    <button id="info_button" data-buttonName="header: Info Button"><span class="sr-only">Info</span></button>
                    <button id="back_button" data-buttonName="header: Back Button"><span class="sr-only">Back</span></button>
                    <button id="home_button" data-buttonName="header: Home Button"><span class="sr-only">Home</span></button>
                    <button id="top_bar_search_button" data-buttonName="header: Search Button"><span class="sr-only">Search</span></button>
                </div>
                <!-- / #page_navigation -->
                <div id="clock">

                </div>
                <!-- / #clock -->
            </div>
            <!-- / .content-row -->
        </div>
        <!-- / .content-container -->
    </div>
    <!-- / #page_header -->

    <div id="page_content">

        <ul class="nav nav-tabs" id="tab_panes_nav" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="init_tab" data-toggle="tab" href="#init" role="tab" aria-controls="init" aria-selected="true">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="session_search_tab" data-toggle="tab" href="#session_search" role="tab" aria-controls="session_search" aria-selected="false">Session Search</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="session_search_results_tab" data-toggle="tab" href="#session_search_results" role="tab" aria-controls="session_search_results" aria-selected="false">Session Search Results</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="venues_tab" data-toggle="tab" href="#venues" role="tab" aria-controls="venues" aria-selected="false">Venues</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="venue_menu_tab" data-toggle="tab" href="#venue_menu" role="tab" aria-controls="venue_menu" aria-selected="false">Venue Menu</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="rooms_tab" data-toggle="tab" href="#rooms" role="tab" aria-controls="rooms" aria-selected="false">Rooms</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="transportation_options_tab" data-toggle="tab" href="#transportation_options" role="tab" aria-controls="transportation_options" aria-selected="false">Transportation Options</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="route_steps_tab" data-toggle="tab" href="#route_steps" role="tab" aria-controls="route_steps" aria-selected="false">Route Steps</a>
            </li>
        </ul>
        <!-- / #tab_panes_nav -->

        <div class="tab-content" id="tab_panes_content">
            <div class="tab-pane fade show active" id="init" role="tabpanel" aria-labelledby="init_tab">
            </div>
            <!-- / #init -->
            <div class="tab-pane fade" id="session_search" role="tabpanel" aria-labelledby="session_search_tab"></div>
            <!-- / #session_search -->
            <div class="tab-pane fade" id="session_search_results" role="tabpanel" aria-labelledby="session_search_results_tab">
                
            </div>
            <!-- / #session_search_results -->
            <div class="tab-pane fade" id="venues" role="tabpanel" aria-labelledby="venues_tab"></div>
            <!-- / #venues -->
            <div class="tab-pane fade" id="venue_menu" role="tabpanel" aria-labelledby="venue_menu_tab"></div>
            <!-- / #venue_menu -->

            <div class="tab-pane fade" id="rooms" role="tabpanel" aria-labelledby="rooms_tab"></div>
            <!-- / #rooms -->
            <div class="tab-pane fade" id="transportation_options" role="tabpanel" aria-labelledby="transportation_options_tab"></div>
            <!-- / #transportation_options -->
            <div class="tab-pane fade" id="route_steps" role="tabpanel" aria-labelledby="route_steps_tab"></div>
            <!-- / #route_steps -->
        </div>
        <!-- / #tab_panes_content -->

    </div>
    <!-- / #page_content -->

    <div id="page_footer">
        <div class="content-container">
            <div class="content-row">
                <div id="footer_navigation">
                    <!-- <button id="ada_button"><span class="sr-only">ADA</span></button> -->

                        <!-- <img src="/assets/img/Icons/ADA.png" class="w-auto p-1 h-50 d-inline-block rounded-circle bg-blue mr-2"> -->
                    <div class="custom-control custom-switch d-inline-block" style="padding-top: 0px;">
                      <input data-active="false" type="checkbox" class="custom-control-input float-right" id="customSwitch1">
                      <label class="custom-control-label float-left" for="customSwitch1">
                      </label>
                    </div>
                </div>
                <div id="ada_page_navigation">
                    <button id="ada_info_button" data-buttonName="footer: ADA Info Button"><span class="sr-only">Info</span></button>
                    <button id="ada_back_button" data-buttonName="footer: ADA Back Button"><span class="sr-only">Back</span></button>
                    <button id="ada_home_button" data-buttonName="footer: ADA Home Button"><span class="sr-only">Home</span></button>
                        <button id="ada_search_button" data-buttonName="footer: ADA Search Button"><span class="sr-only">Search</span></button>
                </div>
                <!-- / #ada_page_navigation -->
            </div>
            <!-- / .content-row -->
        </div>
        <!-- / .content-container -->
    </div>
    <!-- / #page_footer -->

</div>
<!-- / #page_wrapper -->

<script type="text/javascript">
    var helpMessages = {
        "init": {
            "headline": "Select or Search for a Destination",
            "message": "Search for session-specific information or select a type of destination."
        },
        // "session_search": {
        //     "headline": "Search by Session Information",
        //     "message": "Find session-specific information by searching for your desired session."
        // },
        "session_search_results": {
            "headline": "Search Results",
            "message": "The information listed on this page has been returned through your search. By selecting a session, you can get directions to your desired session's room."
        },
        "transportation_options": {
            "headline": "Transportation Method",
            "message": "Select the method of transportation you plan to take to get from your current location to your destination."
        },
        "route_steps": {
            "headline": "Directions to Your Destination",
            "message": "Use these step-by-step directions to navigate to your selected destination. Use a QR code reader on your smart phone to take these directions with you while you travel to your destination."
        },
        // "venues": {
        //     "headline": "Navigate a Venue",
        //     "message": "Select a destination venue you want to navigate to from your current location."
        // },
        "venue_menu": {
            "headline": "Directions to Your Destination",
            "message": "Use these directions to navigate to your selected destination. Use a QR code reader on your smart phone to open a website to take these directions with you while you travel to your destination."
        }
    };

    // console.log(helpMessages['init']);
</script>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body px-5 pb-5">
            <h3 id="headline" class="text-orange mb-4"><strong></strong></h3>
            <p id="message" class="text-orange"></p>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="myOtherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body px-5 pb-5">
            <h3 class="text-orange mb-4"><strong>Accessible Mode is Active</strong></h3>
            <p class="text-orange">Mapped routes will guide you to elevators instead of stairs and escalators throughout the campus.</p>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="myOtherOtherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body px-5 pb-5">
            <h3 class="text-orange mb-4"><strong>Still there?</strong></h3>
            <p class="text-orange">The screen will reset in
            <span id="countdown_timer" style="font-weight: 900"></span> seconds.</p>
            <p class="text-orange">Touch anywhere on the screen to cancel.</p>
         </div>
      </div>
   </div>
</div>



<script src="https://kit.fontawesome.com/0f7866c157.js" crossorigin="anonymous"></script>
