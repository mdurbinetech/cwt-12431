<?php header("HTTP/1.0 404 Not Found");  ?>
<div class="container h-100">
    <div class="row h-100 align-items-center justify-content-center">
        <div class="col col-8">
            <h1 class="h3 text-white">404 - Not Found</h1>
            <p class="text-white">The requested resource does not exist on this server.</p>
        </div>
    </div>
</div class="container">
