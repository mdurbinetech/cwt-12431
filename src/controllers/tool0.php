<?php
/**
 * Tool class for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class ___ToolController extends Controller
{
    public $apiUrlBase = 'http://apps.etechevents.com/amazon/2019/campuswayfinding/';

    public function __construct()
    {
        $this->http = new Http();
        $this->view = new View();
        $this->setControllerName();
    }

    public function directions($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId) {
        $ajaxUrl =  'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT']. '/' . 'ajax/tool/routeSteps/' . $startPointId . '/' . $endPointId .'/'. $transportationMethod .'/'. $adaCompliance .'/'. $venueId;

        $http_req = new Http();
        $resp = $http_req->get($ajaxUrl);

        $this->setViewVariable('directionsRouteStepsContent', $resp);
        $this->setView('tool', 'partials/directionsRouteSteps');
        $this->view->setLayout('directions');
    }

    public function test()
    {
        $this->setView($this->controllerName, 'test');
        $this->view->setLayout('tool');
    }

    public function init()
    {
        $this->setView($this->controllerName, 'partials/init');
        $this->view->setLayout('partial');
    }

    public function sessionSearch($searchTerms = [], $errorText = null)
    {
        $this->setView($this->controllerName, 'partials/sessionSearch');
        $this->view->setLayout('partial');
    }

    public function sessionSearchResults($searchTerms)
    {
        // Initialize empty JSON array
        $sessionSearchResults = '{"SESSIONS": []}';

        // API call
        $sessionSearchResults = $this->getSessionSearchResults($searchTerms);

        // Create array from JSON
        $sessionSearchResults = json_decode($sessionSearchResults, true)['SESSIONS'];

        $this->setView($this->controllerName, 'partials/sessionSearchResults');
        $this->setViewVariable('sessionSearchResults', $sessionSearchResults);
        $this->setViewVariable('searchTerms', $searchTerms);
        $this->view->setLayout('partial');
    }

    public function transportationOptions($startPointId, $endPointId, $endVenueId)
    {
        $transportationOptions = $this->getTransportationOptions($startPointId, $endPointId, $endVenueId);

        $transportationOptions = json_decode($transportationOptions, true);

        // print_r($transporation)

        if(is_array($transportationOptions)){
            if(array_key_exists('TRANSPORTATIONMETHODS', $transportationOptions)){

                $transportationOptions['METHODS'] = $transportationOptions['TRANSPORTATIONMETHODS'];

                $transportationOptions['ENDVENUEID'] = $endVenueId;

                if(count($transportationOptions) > 0){
                    // sort alphabetically by name
                    // usort($transportationOptions, 'compare_name');
                    //
                    // function compare_name($a, $b)
                    // {
                    //     return strnatcmp($a['METHOD'], $b['METHOD']);
                    // }
                }
            }
        } else {
            $transportationOptions = [];
        }






        $this->setView($this->controllerName, 'partials/transportationOptions');
        $this->setViewVariable('transportationOptions', $transportationOptions);
        $this->view->setLayout('partial');
    }

    public function routeSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId)
    {
        $routeStepsAndInfo = $this->getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId);

        // print_r($routeStepsAndInfo);
        // exit();

        if (empty($routeStepsAndInfo)) {
            // Initialize empty JSON array
            $routeSteps = '{"STEPS": []}';
            $routeInfo = '{"STARTPOINT": "", "ENDPOINT": "", "STARTVENUEROOM": ""}';
        } else {
            // Create array from JSON
            $routeSteps = json_decode($routeStepsAndInfo, true)['STEPS'];
            // Define vars from JSON
            $routeInfo["STARTPOINT"] = json_decode($routeStepsAndInfo, true)["STARTPOINT"];
            $routeInfo["ENDPOINT"] = json_decode($routeStepsAndInfo, true)["ENDPOINT"];
            $routeInfo["ENDVENUE"] = json_decode($routeStepsAndInfo, true)["ENDVENUE"];
            $routeInfo["ENDVENUEROOM"] = json_decode($routeStepsAndInfo, true)["ENDVENUEROOM"];
            $routeInfo["STARTVENUE"] = json_decode($routeStepsAndInfo, true)["STARTVENUE"];
            $routeInfo["STARTPOINTID"] = $routeSteps[0]['STARTPOINTID'];
            $routeInfo["STARTVENUEROOM"] = json_decode($routeStepsAndInfo, true)["STARTVENUEROOM"];
            $routeInfo["ENDPOINTID"] = $routeSteps[(count($routeSteps)-1)]['ENDPOINTID'];
        }


        $this->setView($this->controllerName, 'partials/routeSteps');
        $this->setViewVariable('routeSteps', $routeSteps);
        $this->setViewVariable('routeInfo', $routeInfo);

        $routeQrCodeUrlString = 'https://api.qrserver.com/v1/create-qr-code/?size=250x250&data='.'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] . '/directions/test/'.$startPointId . '/' . $endPointId . '/' . $transportationMethod . '/' . $adaCompliance . '/' . $venueId;
        // echo $routeQrCodeUrlString;
        $this->setViewVariable('routeQrCodeUrl', $routeQrCodeUrlString);
        $this->view->setLayout('partial');
    }

    public function rooms($venueId)
    {
        // API Call
        $venueRooms = $this->getRooms($venueId);


        if (empty($venueRooms)) {
            // Initialize empty JSON array
            $venueRooms = '{"VENUE":"", "ROOMS": []}';
        } else {
            // Create array from JSON
            $venueRooms = json_decode($venueRooms, true);
            $venueName = $venueRooms['VENUE'];
            $venueRooms = $venueRooms['ROOMS'];
        }

        // Sort alphabetically by name
        function compare_name($a, $b)
        {
            return strnatcmp($a['ROOMNAME'], $b['ROOMNAME']);
        }

        // sort alphabetically by name
        usort($venueRooms, 'compare_name');

        $this->setView($this->controllerName, 'partials/rooms');
        $this->setViewVariable('venueRooms', $venueRooms);
        $this->setViewVariable('venueId', $venueId);
        $this->setViewVariable('venueName', $venueName);
        $this->view->setLayout('partial');
    }

    public function venues()
    {

        // API CALL
        $venues = $this->getVenues();

        if (empty($venues)) {
            // Initialize empty json array
            $venues = '{"VENUES": []}';
        } else {
            // Create array from JSON
            $venues = json_decode($venues, true)['VENUES'];
        }

        // Sort alphabetically
        function compare_name($a, $b)
        {
            return strnatcmp($a['VENUENAME'], $b['VENUENAME']);
        }

        // sort alphabetically by name
        usort($venues, 'compare_name');

        $this->setView($this->controllerName, 'partials/venues');
        $this->setViewVariable('venues', $venues);
        $this->view->setLayout('partial');
    }

    public function venueMenu($venueId, $startPointId)
    {
        // Initialize empty JSON array
        $venueMenuItems = '{"MENU": []}';

        // API call
        $venueMenuItems = $this->getVenueMenu($venueId, $startPointId);

        // Create array from JSON
        $venueMenuItems = json_decode($venueMenuItems, true)['MENU'];

        $venue = (string) json_decode($this->getVenueMenu($venueId, $startPointId), true)['VENUE'];
        $venueId = (string) json_decode($this->getVenueMenu($venueId, $startPointId), true)['VENUEID'];


        // Sort alphabetically
        function compare_name($a, $b)
        {
            return strnatcmp($a['NAME'], $b['NAME']);
        }

        // sort alphabetically by name
        usort($venueMenuItems, 'compare_name');

        $this->setView($this->controllerName, 'partials/venueMenu');
        $this->setViewVariable('venueMenuItems', $venueMenuItems);
        $this->setViewVariable('venue', $venue);
        $this->setViewVariable('venueId', $venueId);
        $this->view->setLayout('partial');
    }

    public function getSessionSearchResults($searchTerms)
    {
        return $this->http->get($this->apiUrlBase . 'sessions.cfm?SearchStr=' . $searchTerms);
    }

    public function getTransportationOptions($startPointId, $endPointId, $endVenueId)
    {
        return $this->http->get($this->apiUrlBase . 'transportationMethods.cfm?startPointId=' . $startPointId . '&endPointId=' . $endPointId . '&endVenueId=' . $endVenueId);
    }

    public function getRooms($venueId)
    {
        return $this->http->get($this->apiUrlBase . 'rooms.cfm?venueId=' . $venueId);
    }

    public function getVenues()
    {
        return $this->http->get($this->apiUrlBase . 'venues.cfm');
    }

    public function getVenueMenu($venueId, $startPointId)
    {
        return $this->http->get($this->apiUrlBase . 'venues.cfm?venueId=' . $venueId . '&startPointId=' . $startPointId);
    }

    public function getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId)
    {
        // echo $this->apiUrlBase . 'routeSteps.cfm?startPointId='.$startPointId.'&endPointId='.$endPointId.'&adaCompliant='.$adaCompliance.'&tranMode='. $transportationMethod . '&venueId=' . $venueId;
        return $this->http->get($this->apiUrlBase . 'routeSteps.cfm?startPointId='.$startPointId.'&endPointId='.$endPointId.'&adaCompliant='.$adaCompliance.'&tranMode='. $transportationMethod . '&venueId=' . $venueId);
    }

    public function getDirections(){

    }
}
