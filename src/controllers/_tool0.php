<?php
/**
 * Tool class for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class _ToolController extends Controller
{
    public $apiUrlBase = 'http://apps.etechevents.com/amazon/2019/campuswayfinding/';

    public function __construct()
    {
        $this->http = new Http();
        $this->view = new View();
        $this->setControllerName();
    }

    public function test()
    {
        $this->setView($this->controllerName, 'test');
        $this->view->setLayout('tool');
    }

    public function init()
    {
        $this->setView($this->controllerName, 'partials/init');
        $this->view->setLayout('partial');
    }

    public function sessionSearch($searchTerms = [], $errorText = null)
    {
        $this->setView($this->controllerName, 'partials/sessionSearch');
        $this->setViewVariable('errorText', $errorText);
        $this->view->setLayout('partial');
    }

    public function sessionSearchResults($searchTerms)
    {
        // Initialize empty JSON array
        $sessionSearchResults = '{"SESSIONS": []}';

        // API call
        $sessionSearchResults = $this->getSessionSearchResults($searchTerms);

        // Create array from JSON
        $sessionSearchResults = json_decode($sessionSearchResults, true)['SESSIONS'];

        $this->setView($this->controllerName, 'partials/sessionSearchResults');
        $this->setViewVariable('sessionSearchResults', $sessionSearchResults);
        $this->view->setLayout('partial');
    }

    public function transportationOptions($startPointId, $endVenueId)
    {
        $transportationOptions = $this->getTransportationOptions($startPointId, $endVenueId);
        // $transportationOptions = '[]';

        $transportationOptions = json_decode($transportationOptions, true)['TRANSPORTATIONMETHODS'];
        function compare_name($a, $b)
        {
            return strnatcmp($a['METHOD'], $b['METHOD']);
        }

        // sort alphabetically by name
        usort($transportationOptions, 'compare_name');

        $this->setView($this->controllerName, 'partials/transportationOptions');
        $this->setViewVariable('transportationOptions', $transportationOptions);
        $this->view->setLayout('partial');
    }

    public function routeSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance)
    {
        $routeStepsAndInfo = $this->getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance);

        $routeInfo = [];
        $routeSteps = "";

        if (empty($routeStepsAndInfo)) {
            // Initialize empty JSON array
            $routeSteps = '{"STEPS": []}';
            $routeInfo = '{"STARTPOINT": "", "ENDPOINT": ""}';
        } else {
            // Create array from JSON
            $routeSteps = json_decode($routeStepsAndInfo, true)['STEPS'];
            // Define vars from JSON
            $routeInfo["STARTPOINT"] = json_decode($routeStepsAndInfo, true)["STARTPOINT"];
            $routeInfo["ENDPOINT"] = json_decode($routeStepsAndInfo, true)["ENDPOINT"];
        }


        $this->setView($this->controllerName, 'partials/routeSteps');
        $this->setViewVariable('routeSteps', $routeSteps);
        $this->setViewVariable('routeInfo', $routeInfo);
    }

    public function rooms($venueId)
    {
        // API Call
        $venueRooms = $this->getRooms($venueId);

        if (empty($venueRooms)) {
            // Initialize empty JSON array
            $venueRooms = '{"ROOMS": []}';
        } else {
            // Create array from JSON
            $venueRooms = json_decode($venueRooms, true)['ROOMS'];
        }

        // Sort alphabetically by name
        function compare_name($a, $b)
        {
            return strnatcmp($a['ROOMNAME'], $b['ROOMNAME']);
        }

        // sort alphabetically by name
        usort($venueRooms, 'compare_name');

        $this->setView($this->controllerName, 'partials/rooms');
        $this->setViewVariable('venueRooms', $venueRooms);
        $this->view->setLayout('partial');
    }

    public function venues()
    {

        // API CALL
        $venues = $this->getVenues();

        if (empty($venues)) {
            // Initialize empty json array
            $venues = '{"VENUES": []}';
        } else {
            // Create array from JSON
            $venues = json_decode($venues, true)['VENUES'];
        }

        // Sort alphabetically
        function compare_name($a, $b)
        {
            return strnatcmp($a['VENUENAME'], $b['VENUENAME']);
        }

        // sort alphabetically by name
        usort($venues, 'compare_name');

        $this->setView($this->controllerName, 'partials/venues');
        $this->setViewVariable('venues', $venues);
        $this->view->setLayout('partial');
    }

    public function venueMenu($venueId)
    {
        // Initialize empty JSON array
        $venueMenuItems = '{"MENU": []}';

        // API call
        $venueMenuItems = $this->getVenueMenu($venueId);

        // Create array from JSON
        $venueMenuItems = json_decode($venueMenuItems, true)['MENU'];

        // Sort alphabetically
        function compare_name($a, $b)
        {
            return strnatcmp($a['NAME'], $b['NAME']);
        }

        // sort alphabetically by name
        usort($venueMenuItems, 'compare_name');

        $this->setView($this->controllerName, 'partials/venueMenu');
        $this->setViewVariable('venueMenuItems', $venueMenuItems);
        $this->view->setLayout('partial');
    }

    public function getSessionSearchResults($searchTerms)
    {
        return $this->http->get($this->apiUrlBase . 'sessions.cfm?SearchStr=' . $searchTerms);
    }

    public function getTransportationOptions($startPointId, $endVenueId)
    {
        return $this->http->get($this->apiUrlBase . 'transportationMethods.cfm?startPointId=' . $startPointId . '&endVenueId=' . $endVenueId);
    }

    public function getRooms($venueId)
    {
        return $this->http->get($this->apiUrlBase . 'rooms.cfm?venueId=' . $venueId);
    }

    public function getVenues()
    {
        return $this->http->get($this->apiUrlBase . 'venues.cfm');
    }

    public function getVenueMenu($venueId)
    {
        return $this->http->get($this->apiUrlBase . 'venues.cfm?venueId=' . $venueId);
    }

    public function getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance)
    {
        return $this->http->get($this->apiUrlBase . 'routeSteps.cfm?startPointId='.$startPointId.'&endPointId='.$endPointId.'&adaCompliant='.$adaCompliance.'&tranMode='. $transportationMethod);
    }
}
