<?php
/**
 * Map controller for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class MapController extends Controller
{
    public function get($mapFile, $startPointId, $endPointId)
    {
        $map = new Map($mapFile, $startPointId, $endPointId, $startPointId . '-' . $endPointId);
        return $map;
    }

    public function svgPathBounds($mapFile, $pathId) {
        $this->setView('tool', 'partials/svgPathBounds');
        $this->setViewVariable('mapFile', $mapFile);
        $this->setViewVariable('pathId', $pathId);
        $this->view->setLayout('partial');
    }

    public function getSvgPathBounds($mapFile, $pathId) {
        return 0;
    }

    public function renderMap(){
        $map->render();
    }
}
