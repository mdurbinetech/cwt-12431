<?php
/**
 * Tool class for application
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

class ToolController extends Controller
{
    public $apiUrlBase = 'http://52.52.88.146/';

    public function __construct()
    {
        $this->http = new Http();
        $this->view = new View();
        $this->setControllerName();
    }

    public function directions($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId)
    {
        $routeStepsAndInfo = $this->getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId);

        // print_r($routeStepsAndInfo);
        // exit();

        if (empty($routeStepsAndInfo)) {
            // Initialize empty JSON array
            $routeSteps = '{"STEPS": []}';
            $routeInfo = '{"STARTPOINT": "", "ENDPOINT": "", "STARTVENUEROOM": ""}';
        } else {
            // Create array from JSON
            $routeSteps = json_decode($routeStepsAndInfo, true)['STEPS'];
            // Define vars from JSON
            $routeInfo["STARTPOINT"] = json_decode($routeStepsAndInfo, true)["STARTPOINT"];
            $routeInfo["ENDPOINT"] = json_decode($routeStepsAndInfo, true)["ENDPOINT"];
            $routeInfo["ENDVENUE"] = json_decode($routeStepsAndInfo, true)["ENDVENUE"];
            $routeInfo["ENDVENUEROOM"] = json_decode($routeStepsAndInfo, true)["ENDVENUEROOM"];
            $routeInfo["STARTVENUE"] = json_decode($routeStepsAndInfo, true)["STARTVENUE"];
            $routeInfo["STARTPOINTID"] = $routeSteps[0]['STARTPOINTID'];
            $routeInfo["STARTVENUEROOM"] = json_decode($routeStepsAndInfo, true)["STARTVENUEROOM"];
            $routeInfo["ENDPOINTID"] = $routeSteps[(count($routeSteps)-1)]['ENDPOINTID'];
        }


        $this->setView($this->controllerName, 'partials/directionsRouteSteps');
        $this->setViewVariable('routeSteps', $routeSteps);
        $this->setViewVariable('routeInfo', $routeInfo);

        $this->view->setLayout('directions');
    }

    public function directionsFromUniqueName($uniqueName)
    {
        $routeStepsAndInfo = $this->getDirectionsInfoFromUniqueName($uniqueName);

        // print_r($routeStepsAndInfo);
        // exit();

        if (empty($routeStepsAndInfo)) {
            // Initialize empty JSON array
            $routeSteps = '{"STEPS": []}';
            $routeInfo = '{"STARTPOINT": "", "ENDPOINT": "", "STARTVENUEROOM": ""}';
        } else {
            // Create array from JSON
            $routeSteps = json_decode($routeStepsAndInfo, true)['STEPS'];
            // Define vars from JSON
            $routeInfo["STARTPOINT"] = json_decode($routeStepsAndInfo, true)["STARTPOINT"];
            $routeInfo["ENDPOINT"] = json_decode($routeStepsAndInfo, true)["ENDPOINT"];
            $routeInfo["ENDVENUE"] = json_decode($routeStepsAndInfo, true)["ENDVENUE"];
            $routeInfo["ENDVENUEROOM"] = json_decode($routeStepsAndInfo, true)["ENDVENUEROOM"];
            $routeInfo["STARTVENUE"] = json_decode($routeStepsAndInfo, true)["STARTVENUE"];
            $routeInfo["STARTPOINTID"] = $routeSteps[0]['STARTPOINTID'];
            $routeInfo["STARTVENUEROOM"] = json_decode($routeStepsAndInfo, true)["STARTVENUEROOM"];
            $routeInfo["ENDPOINTID"] = $routeSteps[(count($routeSteps)-1)]['ENDPOINTID'];
        }


        $this->setView($this->controllerName, 'partials/directionsRouteSteps');
        $this->setViewVariable('routeSteps', $routeSteps);
        $this->setViewVariable('routeInfo', $routeInfo);

        $this->view->setLayout('directions');
    }

    public function test()
    {
        $this->setView($this->controllerName, 'test');
        $this->view->setLayout('tool');
    }

    public function init()
    {
        $this->setView($this->controllerName, 'partials/init');
        $this->view->setLayout('partial');
    }

    public function newHome($startPointId, $adaCompliance)
    {
        $this->setView($this->controllerName, 'partials/newHome');
        $this->view->setLayout('partial');

        $whereDoYouWantToGo = $this->getWhereDoYouWantToGo($startPointId, $adaCompliance);
        $this->setViewVariable('whereDoYouWantToGo', $whereDoYouWantToGo);
    }

    public function pickVenues($startPointId, $adaCompliance, $endOption, $endVenueId = null)
    {
        if ($endVenueId == null) {
            // echo $endOption;
            // exit();
            $pickVenues = $this->getpickVenues($startPointId, $adaCompliance, $endOption);
            // print_r($pickVenues);
            // exit();
            $this->setView($this->controllerName, 'partials/pickVenues');
            $this->view->setLayout('partial');


            $this->setViewVariable('pickVenues', $pickVenues);
        } else {
            $pickVenues = $this->getPickVenuesForMeals($startPointId, $adaCompliance, $endOption, $endVenueId);
            // print_r($pickVenues);

            // echo $endVenueId . '<br>';
            // exit();
            $this->setView($this->controllerName, 'partials/pickVenues');
            $this->view->setLayout('partial');


            $this->setViewVariable('pickVenues', $pickVenues);
        }
    }

    public function sessionSearch($searchTerms = [], $errorText = null)
    {
        $this->setView($this->controllerName, 'partials/sessionSearch');
        $this->view->setLayout('partial');
    }

    public function sessionSearchResults($searchTerms)
    {
        // Initialize empty JSON array
        $sessionSearchResults = '{"SESSIONS": []}';

        // API call
        $sessionSearchResults = $this->getSessionSearchResults($searchTerms);

        // print_r($sessionSearchResults);
        // exit();

        // Create array from JSON
        $sessionSearchResults = json_decode($sessionSearchResults, true)['SESSIONS'];

        $this->setView($this->controllerName, 'partials/sessionSearchResults');
        $this->setViewVariable('sessionSearchResults', $sessionSearchResults);
        $this->setViewVariable('searchTerms', str_replace('%20', ' ', $searchTerms));
        $this->view->setLayout('partial');
    }

    public function transportationOptions($startPointId, $endPointId, $endVenueId)
    {
        $transportationOptions = $this->getTransportationOptions($startPointId, $endPointId, $endVenueId);

        $transportationOptions = json_decode($transportationOptions, true);

        // print_r($transporation)

        if (is_array($transportationOptions)) {
            if (array_key_exists('TRANSPORTATIONMETHODS', $transportationOptions)) {
                $transportationOptions['METHODS'] = $transportationOptions['TRANSPORTATIONMETHODS'];

                $transportationOptions['ENDVENUEID'] = $endVenueId;

                if (count($transportationOptions) > 0) {
                    // sort alphabetically by name
                    // usort($transportationOptions, 'compare_name');
                    //
                    // function compare_name($a, $b)
                    // {
                    //     return strnatcmp($a['METHOD'], $b['METHOD']);
                    // }
                }
            }
        } else {
            $transportationOptions = [];
        }






        $this->setView($this->controllerName, 'partials/transportationOptions');
        $this->setViewVariable('transportationOptions', $transportationOptions);
        $this->view->setLayout('partial');
    }

    public function directionsRouteSteps($routeStepsAndInfo)
    {
        if (empty($routeStepsAndInfo)) {
            // Initialize empty JSON array
            $routeSteps = '{"STEPS": []}';
            $routeInfo = '{"STARTPOINT": "", "ENDPOINT": "", "STARTVENUEROOM": ""}';
        } else {
            // Create array from JSON
            $routeSteps = json_decode($routeStepsAndInfo, true)['STEPS'];
            // Define vars from JSON
            $routeInfo["STARTPOINT"] = json_decode($routeStepsAndInfo, true)["STARTPOINT"];
            $routeInfo["ENDPOINT"] = json_decode($routeStepsAndInfo, true)["ENDPOINT"];
            $routeInfo["ENDVENUE"] = json_decode($routeStepsAndInfo, true)["ENDVENUE"];
            $routeInfo["ENDVENUEROOM"] = json_decode($routeStepsAndInfo, true)["ENDVENUEROOM"];
            $routeInfo["STARTVENUE"] = json_decode($routeStepsAndInfo, true)["STARTVENUE"];
            $routeInfo["STARTPOINTID"] = $routeSteps[0]['STARTPOINTID'];
            $routeInfo["STARTVENUEROOM"] = json_decode($routeStepsAndInfo, true)["STARTVENUEROOM"];
            $routeInfo["ENDPOINTID"] = $routeSteps[(count($routeSteps)-1)]['ENDPOINTID'];
        }


        $this->setView($this->controllerName, 'partials/directionsRouteSteps');
        $this->setViewVariable('routeSteps', $routeSteps);
        $this->setViewVariable('routeInfo', $routeInfo);

        $this->view->setLayout('directions');
    }

    public function routeSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId)
    {
        $routeStepsAndInfo = $this->getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId);

        // print_r(json_decode($routeStepsAndInfo, true)["UNIQUENAME"]);
        // exit();

        if (empty($routeStepsAndInfo)) {
            // Initialize empty JSON array
            $routeSteps = '{"STEPS": []}';
            $routeInfo = '{"STARTPOINT": "", "ENDPOINT": "", "STARTVENUEROOM": "", "UNIQUENAME": ""}';
        } else {
            // Create array from JSON
            $routeSteps = json_decode($routeStepsAndInfo, true)['STEPS'];
            // Define vars from JSON
            $routeInfo["STARTPOINT"] = json_decode($routeStepsAndInfo, true)["STARTPOINT"];
            $routeInfo["ENDPOINT"] = json_decode($routeStepsAndInfo, true)["ENDPOINT"];
            $routeInfo["ENDVENUE"] = json_decode($routeStepsAndInfo, true)["ENDVENUE"];
            $routeInfo["ENDVENUEROOM"] = json_decode($routeStepsAndInfo, true)["ENDVENUEROOM"];
            $routeInfo["STARTVENUE"] = json_decode($routeStepsAndInfo, true)["STARTVENUE"];
            $routeInfo["STARTPOINTID"] = $routeSteps[0]['STARTPOINTID'];
            $routeInfo["STARTVENUEROOM"] = json_decode($routeStepsAndInfo, true)["STARTVENUEROOM"];
            $routeInfo["ENDPOINTID"] = $routeSteps[(count($routeSteps)-1)]['ENDPOINTID'];
            $routeInfo["UNIQUENAME"] = json_decode($routeStepsAndInfo, true)["UNIQUENAME"];
        }


        $this->setView($this->controllerName, 'partials/routeSteps');
        $this->setViewVariable('routeSteps', $routeSteps);
        $this->setViewVariable('routeInfo', $routeInfo);

        // $routeQrCodeUrlString = 'https://api.qrserver.com/v1/create-qr-code/?size=500x500&data='.'http://'.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] . '/directions/test/'.json_decode($routeStepsAndInfo, true)["UNIQUENAME"];
        // echo $routeQrCodeUrlString;
        $routeQrCodeUrlString = PROTOCOL.$_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'] . '/directions/test/'.json_decode($routeStepsAndInfo, true)["UNIQUENAME"];
        $this->setViewVariable('routeQrCodeUrl', $routeQrCodeUrlString);
        $this->view->setLayout('partial');
    }

    public function rooms($startPointId, $adaCompliance, $venueId)
    {
        // API Call
        $venueRooms = $this->getRooms($startPointId, $adaCompliance, $venueId);

        // print_r($venueRooms);
        // exit();


        if (empty($venueRooms)) {
            // Initialize empty JSON array
            $venueRooms = '{"VENUE":"", "ROOMS": []}';
        } else {
            // Create array from JSON
            $venueRooms = json_decode($venueRooms, true);
            $venueName = $venueRooms['LOCATIONS'][0]['VENUENAME'];
            // $venueName = '';
            $venueRooms = $venueRooms['LOCATIONS'];
        }

        // Sort alphabetically by name
        function compare_name($a, $b)
        {
            return strnatcmp($a['PRETTYNAME'], $b['PRETTYNAME']);
        }

        // sort alphabetically by name
        usort($venueRooms, 'compare_name');

        $this->setView($this->controllerName, 'partials/rooms');
        $this->setViewVariable('venueRooms', $venueRooms);
        $this->setViewVariable('venueId', $venueId);
        $this->setViewVariable('venueName', $venueName);
        $this->view->setLayout('partial');
    }

    public function venues()
    {

        // API CALL
        $venues = $this->getVenues();

        if (empty($venues)) {
            // Initialize empty json array
            $venues = '{"VENUES": []}';
        } else {
            // Create array from JSON
            $venues = json_decode($venues, true)['VENUES'];
        }

        // Sort alphabetically
        function compare_name($a, $b)
        {
            return strnatcmp($a['VENUENAME'], $b['VENUENAME']);
        }

        // sort alphabetically by name
        usort($venues, 'compare_name');

        $this->setView($this->controllerName, 'partials/venues');
        $this->setViewVariable('venues', $venues);
        $this->view->setLayout('partial');
    }

    public function venueMenu($venueId, $startPointId)
    {
        // Initialize empty JSON array
        $venueMenuItems = '{"MENU": []}';

        // API call
        $venueMenuItems = $this->getVenueMenu($venueId, $startPointId);

        // Create array from JSON
        $venueMenuItems = json_decode($venueMenuItems, true)['MENU'];

        $venue = (string) json_decode($this->getVenueMenu($venueId, $startPointId), true)['VENUE'];
        $venueId = (string) json_decode($this->getVenueMenu($venueId, $startPointId), true)['VENUEID'];


        // Sort alphabetically
        function compare_name($a, $b)
        {
            return strnatcmp($a['NAME'], $b['NAME']);
        }

        // sort alphabetically by name
        usort($venueMenuItems, 'compare_name');

        $this->setView($this->controllerName, 'partials/venueMenu');
        $this->setViewVariable('venueMenuItems', $venueMenuItems);
        $this->setViewVariable('venue', $venue);
        $this->setViewVariable('venueId', $venueId);
        $this->view->setLayout('partial');
    }

    public function qrCode($data)
    {
        $this->setView($this->controllerName, 'partials/qrcode');
        $this->setViewVariable('data', $data);
        $this->view->setLayout('partial');
    }

    public function getSessionSearchResults($searchTerms)
    {
        // echo $this->apiUrlBase . 'sessions.cfm?SearchStr=' . $searchTerms;
        // exit();
        return $this->http->get($this->apiUrlBase . 'sessions.cfm?SearchStr=' . $searchTerms);
    }

    public function getTransportationOptions($startPointId, $endPointId, $endVenueId)
    {
        return $this->http->get($this->apiUrlBase . 'transportationMethods.cfm?startPointId=' . $startPointId . '&endPointId=' . $endPointId . '&endVenueId=' . $endVenueId);
    }

    public function getRooms($startPointId, $adaCompliance, $venueId)
    {
        // return $this->http->get($this->apiUrlBase . 'rooms.cfm?venueId=' . $venueId);

        // echo 'api_PickRoom.cfm?StartPointID='.$startPointId.'&ADACompliant='.$adaCompliance.'&VenueID=' . $venueId;

        // exit();
        return $this->http->get($this->apiUrlBase . 'api_PickRoom.cfm?StartPointID='.$startPointId.'&ADACompliant='.$adaCompliance.'&VenueID=' . $venueId);
    }

    public function getVenues()
    {
        return $this->http->get($this->apiUrlBase . 'venues.cfm');
    }

    public function getVenueMenu($venueId, $startPointId)
    {
        return $this->http->get($this->apiUrlBase . 'venues.cfm?venueId=' . $venueId . '&startPointId=' . $startPointId);
    }

    public function getRouteSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId)
    {
        // echo $this->apiUrlBase . 'routeSteps.cfm?startPointId='.$startPointId.'&endPointId='.$endPointId.'&adaCompliant='.$adaCompliance.'&tranMode='. $transportationMethod . '&venueId=' . $venueId;
        // exit();
        return $this->http->get($this->apiUrlBase . 'routeSteps.cfm?startPointId='.$startPointId.'&endPointId='.$endPointId.'&adaCompliant='.$adaCompliance.'&tranMode='. $transportationMethod . '&venueId=' . $venueId);
    }

    public function getWhereDoYouWantToGo($startPointId, $adaCompliance)
    {
        // echo json_decode($this->http->get($this->apiUrlBase . 'api_WhereDoYOuWantToGo.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance), true);
        // exit();
        return json_decode($this->http->get($this->apiUrlBase . 'api_WhereDoYOuWantToGo.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance), true);
    }

    public function getDirections()
    {
    }

    public function getPickVenues($startPointId, $adaCompliance, $endOption)
    {
        // echo 'test';
        // echo $this->apiUrlBase . 'api_PickVenue.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance. '&endOption='.str_replace('_', ' ', $endOption);

        // exit();
        return json_decode($this->http->get($this->apiUrlBase . 'api_PickVenue.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance. '&endOption='.str_replace('_', '%20', $endOption)), true);
    }

    public function getPickVenuesForMeals($startPointId, $adaCompliance, $endOption, $venueId)
    {
        // echo 'test2';
        // exit();
        // echo $endOption . '<br>';
        // echo $this->apiUrlBase . 'api_PickVenue.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance. '&endOption='.$endOption.'&endVenueId='.$venueId;

        // echo $this->apiUrlBase . 'api_PickVenue.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance. '&endOption='.$endOption.'&VenueId='.$venueId;

        // echo $venueId;
        // exit();

        // exit();
        return json_decode($this->http->get($this->apiUrlBase . 'api_PickVenue.cfm?startPointId='.$startPointId.'&adaCompliant='.$adaCompliance. '&endOption='.$endOption.'&VenueId='.$venueId), true);
    }

    public function getDirectionsInfoFromUniqueName($uniqueName)
    {
        return $this->http->get($this->apiUrlBase . 'route.cfm?code='.$uniqueName);
        // $this->directions($routeInfo['STARTPOINTID'], $routeInfo['ENDPOINTID'], $routeInfo['TRANSPORTATIONMETHOD'], $routeInfo['ADACOMPLIANT'], $routeInfo['ENDVENUEID']);
    }
}
