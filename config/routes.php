<?php
/**
 * Application routing configuration file
 *
 * Defines accessible real and virtual directories within the Web server
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

 // Create Router object
 $router = new Router();

 // create custom handler for alphanumeric with underscores
 $router->addMatchTypes(array('mId' => '[A-Za-z_0-9]+'));
 // $router->addMatchTypes(array('wSpaces' => '[a-zA-Z0-9\/_\-\s]'));

 $pattern = '\x{0750}-\x{077F}';
         // Alphanumeric, /, _, - and space characters
$router->addMatchTypes(array('wUnderscores' => '[A-Za-z_]+'));

 // Map application home page
 $router->get('/', function () {
     (new Controller)->notFound();
 });

 // Map application tool page
 // $router->get('/tool', function () {
 //     (new ToolController)->index();
 // });



 // Map application ajax directions page
 // $router->get('/directions/test/[i:startPointId]/[i:endPointId]/[a:transportationMethod]/[a:adaCompliant]/[*:venueId]', function ($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId= 0) {
 //     (new ToolController)->directions($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId);
 // });

 // Map application ajax tool page - sessionSearch
 $router->get('/directions/test/[a:uniqueName]', function ($uniqueName) {
     (new ToolController)->directionsFromUniqueName($uniqueName);
     // echo 'test';
 });


 // Map application qr code page
 $router->get('/tool/test/qrcode/[*:data]', function ($data) {
     (new ToolController)->qrCode($data);
 });


 // Map application tool test page
 $router->get('/tool/test', function () {
     (new ToolController)->test();
 });

 // Map application tool test init partial
 $router->get('/ajax/tool/init', function () {
     (new ToolController)->init();
 });

 // Map application tool test newHome partial
 $router->get('/ajax/tool/newHome/[i:startPointId]/[i:adaCompliance]', function ($startPointId, $adaCompliance) {
     (new ToolController)->newHome($startPointId, $adaCompliance);
 });

 // Map application tool pickVenues partial for meals
 $router->get('/ajax/tool/pickVenues/[i:startPointId]/[i:adaCompliance]/[wUnderscores:endOption]/[i:venueId]', function ($startPointId, $adaCompliance, $endOption, $venueId) {
     // echo $venueId;
     (new ToolController)->pickVenues($startPointId, $adaCompliance, $endOption, $venueId);
 });

 // Map application tool pickVenues partial
 $router->get('/ajax/tool/pickVenues/[i:startPointId]/[i:adaCompliance]/[wUnderscores:endOption]', function ($startPointId, $adaCompliance, $endOption) {
     // echo 'test';
     // exit();
     (new ToolController)->pickVenues($startPointId, $adaCompliance, $endOption);
 });



 // Map application ajax tool page - rooms
 $router->get('/ajax/tool/rooms/[i:startPointId]/[i:adaCompliance]/[i:venueId]', function ($startPointId, $adaCompliance, $venueId) {
     (new ToolController)->rooms($startPointId, $adaCompliance, $venueId);
 });

 // Map application ajax tool page - transportationOptions
 $router->get('/ajax/tool/transportationOptions/[i:startPointId]/[i:endPointId]/[i:endVenueId]', function ($startPointId, $endPointId, $endVenueId) {
     (new ToolController)->transportationOptions($startPointId, $endPointId, $endVenueId);
 });

 // Map application ajax tool page - venues
 $router->get('/ajax/tool/venues', function () {
     (new ToolController)->venues();
 });

 // Map application ajax tool page - venues
 $router->get('/ajax/tool/venues/[i:venueId]/[i:startPointId]', function ($venueId, $startPointId) {
     (new ToolController)->venueMenu($venueId, $startPointId);
 });

 // Map application ajax tool page - venues
 $router->get('/ajax/tool/svgPathBounds/[*:mapFile]', function ($venueId, $startPointId) {
     (new ToolController)->venueMenu($venueId, $startPointId);
 });

 // Map application ajax tool page - venueMenu
 $router->get('/ajax/tool/routeSteps/[i:startPointId]/[i:endPointId]/[a:transportationMethod]/[a:adaCompliant]', function ($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId= 0) {
     (new ToolController)->routeSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId);
 });

 $router->get('/ajax/tool/routeSteps/[i:startPointId]/[i:endPointId]/[a:transportationMethod]/[a:adaCompliant]/[*:venueId]', function ($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId) {
     (new ToolController)->routeSteps($startPointId, $endPointId, $transportationMethod, $adaCompliance, $venueId);
 });

 // Map application ajax tool page - routeSteps
 $router->get('/ajax/tool/rooms/[i:startPointId]/[i:adaCompliance]/[i:venueId]', function ($startPointId, $adaCompliance, $venueId) {
     (new ToolController)->rooms($startPointId, $adaCompliance, $venueId);
 });

 // Map application ajax tool page - sessionSearch
 $router->get('/ajax/tool/sessionSearch', function () {
     (new ToolController)->sessionSearch();
 });

 // Map application ajax tool page - sessionSearch
 $router->get('/ajax/tool/sessionSearchResults/[*:searchTermsStr]', function ($searchTerms) {
     (new ToolController)->sessionSearchResults($searchTerms);
 });

 // Map application directions page
 // $router->get('/directions', function () {
 //     (new DirectionsController)->index();
 // });

 // Map svg bounds for a path element
 $router->get('/api/map/svgPathBounds/[*:mapFile]/[*:pathId]', function ($mapFile, $pathId) {
     (new MapController)->svgPathBounds($mapFile, $pathId);
 });

 // Map application directions page
 $router->get('/api/map/[*:mapFile]/[*:startPointId]/[*:endPointId]', function ($mapFile, $startPointId, $endPointId) {
     // (new MapController)->get($mapFile, $startPointId, $endPointId);
     $mapControl = new MapController();
     $map = $mapControl->get($mapFile, $startPointId, $endPointId, $startPointId . '-' . $endPointId);
     $mapControl->setViewLayout('map');
     $map->render();
 });



 // Map application 404 page
 $router->get('/404', function () {
     (new Controller)->notFound();
 });

 // Map application 403 page
 $router->get('/403', function () {
     (new Controller)->unauthorized();
 });

// Define match from router uri
$match = $router->match();

// determine if route is a match to a resource
if ($match && is_callable($match['target'])) {
    // display matched resource
    call_user_func_array($match['target'], $match['params']);
} else {
    // no route was matched, show 404
    // header('Location: /404');
}
