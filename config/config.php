<?php
/**
 * Application configuration file
 *
 * Defines application constant variables
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access private
 *
**/

// Establish error logging settings
error_reporting(E_ALL); // Error engine - always TRUE!
ini_set('ignore_repeated_errors', true); // always TRUE
ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");
ini_set('display_errors', true); // Set to FALSE in production environment
ini_set('log_errors_max_len', 1024); // Logging file size

// Application root directory
define('ROOT', __DIR__ . '/../');

// Application configuration directory
define('CONFIG', __DIR__ . '/../config');

// Application public (Web-accessible) directory
define('PUBLIC', __DIR__ . '/../public');

// Application public (Web-accessible) directory
define('ASSETS', __DIR__ . '/../public/assets');

// Application public (Web-accessible) directory
define('CSS', __DIR__ . '/../public/css');

// Application resources directory
define('RESOURCES', __DIR__ . '/../resources');

// Application source directory
define('SRC', __DIR__ . '/../src');

// Application core directory
define('CORE', __DIR__ . '/../src/_core');

// Application models directory
define('MODELS', __DIR__ . '/../src/models');

// Application views directory
define('VIEWS', __DIR__ . '/../src/views');

// Application views directory
define('LAYOUTS', __DIR__ . '/../src/views/layouts');

// Application controllers directory
define('CONTROLLERS', __DIR__ . '/../src/controllers');

// Application maps directory
define('MAPS', __DIR__ . '/../maps');

// Application default page title
define('DEFAULT_PAGE_TITLE', 'AWS re:Invent 2019 Campus Navigation');

// Database connection information
define('DB_HOST', '192.168.0.55');
define('DB_USER', 'sa');
define('DB_PASS', 'Orlando42');
define('DB_NAME', 'AWS_Campus_Wayfinding');


if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    define('PROTOCOL', 'https://');
} else {
    define('PROTOCOL', 'http://');
}
