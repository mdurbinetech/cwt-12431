<?php
/**
 * Public-facing application root
 *
 * Imports application bootstrap file and does nothing else
 *
 * @package cwt-12431
 * @author Mark Durbin - mdurbin@etechevents.com
 * @version 0.0
 * @since 0.0
 * @access public
 *
**/

// require the application bootstrap
require_once(__DIR__ . '/../app.php');
