$(document).ready(function() {
    // A $( document ).ready() block.

    initialize();

    // $(document).on("keypress", '#session_search_field', function(event) {
    //
    //     if( $('#session_search_field').val().length >= 0 ) {
    //         console.log('test');
    //           $('#clear_button').css('visibility', 'visible');
    //     }
    // });


    $(document).on("click", 'button', function(event) {
        $('button').blur();
    });

    $(document).on("click", '.loads-session_search', function(event) {
        event.preventDefault();
        addPanelHistoryItem('session_search');
        showPanel('session_search');
    });

    $('#routeStepsCarousel').carousel('pause');


    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-session_search_results', function(event) {
        event.preventDefault();
        if ($('#session_search_field').val().trim() == '') {
            // TODO reload search page w/error text
        } else {
            ajaxGet('/ajax/tool/sessionSearchResults/' + $('#session_search_field').val().trim(), function(data) {
                replaceInnerHtml('session_search_results', data, showPanel('session_search_results'));
            });
            addPanelHistoryItem('session_search_results');
        }
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-rooms', function(event) {
        event.preventDefault();
        console.log('/ajax/tool/rooms/' + $(this).attr('data-venueId'));
        ajaxGet('/ajax/tool/rooms/' + $(this).attr('data-venueId'), function(data) {
            replaceInnerHtml('rooms', data,showPanel('rooms'));
        });
        addPanelHistoryItem('rooms');
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-venues', function(event) {
        event.preventDefault();
        ajaxGet('/ajax/tool/venues', function(data) {
            replaceInnerHtml('venues', data,
            showPanel('venues'));
        });
        addPanelHistoryItem('venues');
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.venue-button', function(event) {
        event.preventDefault();
        ajaxGet('/ajax/tool/venues/' + $(this).attr('data-venueId') + '/' + getUrlParameter('startPointId'), function(data) {
            replaceInnerHtml('venue_menu', data,
            showPanel('venue_menu'));
        });
        addPanelHistoryItem('venue_menu');
    });

    $(document).on("click", '#back_button', function(event) {
        event.preventDefault();
        let reverseArray = panelHistory.slice();
        reverseArray = reverseArray.reverse();
        let targetPanel = reverseArray[1];
        showPanel(targetPanel);
        removePanelHistoryItem();
    });

    $(document).on("click", '.back-button', function(event) {
        event.preventDefault();
        let reverseArray = panelHistory.slice();
        reverseArray = reverseArray.reverse();
        let targetPanel = reverseArray[1];
        showPanel(targetPanel);
        removePanelHistoryItem();
    });

    $(document).on("click", '#ada_back_button', function(event) {
        event.preventDefault();
        let reverseArray = panelHistory.slice();
        reverseArray = reverseArray.reverse();
        let targetPanel = reverseArray[1];
        showPanel(targetPanel);
        removePanelHistoryItem();
    });

    $(document).on("click", '.session-select-button', function(event) {
        event.preventDefault();
        this_elem = $(this);
        ajaxGet('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'), function(data) {
            if (data.trim().length == 0) {

                ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + 'walk' + '/'+window.localStorage.getItem('cwt-12431_ada', 1)+'/' + $(this).attr('data-venueId') , function(data) {
                    replaceInnerHtml('route_steps', data, showPanel('route_steps'));
                });
                addPanelHistoryItem('route_steps');
            } else {
                replaceInnerHtml('transportation_options', data, showPanel('transportation_options'));
                addPanelHistoryItem('transportation_options');

            }
        });
    });

    $(document).on("click", '.room-select-button', function(event) {
        event.preventDefault();
        this_elem = $(this);
        ajaxGet('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'), function(data) {
            if (data.trim().length == 0) {

                ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + 'walk' + '/'+window.localStorage.getItem('cwt-12431_ada', 1)+'/' + $(this).attr('data-venueId') , function(data) {
                    replaceInnerHtml('route_steps', data, showPanel('route_steps'));
                });
                addPanelHistoryItem('route_steps');
            } else {
                replaceInnerHtml('transportation_options', data, showPanel('transportation_options'));
                addPanelHistoryItem('transportation_options');
            }
        });
    });

    $(document).on("click", '.loads-route-steps', function(event) {
        event.preventDefault();
        this_elem = $(this);
        ajaxGet('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'), function(data) {
            if (data.trim().length == 0) {

                ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + 'walk' + '/'+window.localStorage.getItem('cwt-12431_ada', 1)+'/' + $(this_elem).attr('data-venueId'), function(data) {
                    replaceInnerHtml('route_steps', data,
                    showPanel('route_steps'));
                });
                addPanelHistoryItem('route_steps');
            } else {
                replaceInnerHtml('transportation_options', data);
                addPanelHistoryItem('transportation_options');
                showPanel('transportation_options');
            }
        });
    });

    $(document).on("click", '.transportation-option-select-button', function(event) {
        event.preventDefault();
        // this_elem = $(this);
        ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-transportationMode') + '/'+window.localStorage.getItem('cwt-12431_ada', 1)+'/' + $(this).attr('data-venueId'), function(data) {


            if (data.trim().length == 0) {

                initialize();
            } else {
                replaceInnerHtml('route_steps', data, showPanel('route_steps'));
                addPanelHistoryItem('route_steps');

            }


        });
    });

    $(document).on("click", '#home_button', function(event) {
        initialize();
    });

    $(document).on("click", '#ada_home_button', function(event) {
        panelHistory = [];
        clock();
        showPanel('init');
        window.localStorage.setItem('cwt-12431_ada', 1);
        $('#ada_page_navigation').css('visibility', 'visible');
        $('#page_navigation').css('visibility', 'hidden');
        // load init tab
        ajaxGet('/ajax/tool/init', function(data) {
            replaceInnerHtml('init', data);
            addPanelHistoryItem('init');
        });
        // load session search tab
        ajaxGet('/ajax/tool/sessionSearch', function(data) {
            replaceInnerHtml('session_search', data);
        });
        window.localStorage.setItem('cwt-12431_ada', 1);
    });


    $(document).on("click", '#info_button', function(event) {

        $('#myModal').modal('toggle');
    });

    $(document).on("click", '#ada_info_button', function(event) {

        $('#myModal').modal('toggle');
    });

    $('#myModal').on('show.bs.modal', function (e) {
        currentPanel = panelHistory[(panelHistory.length - 1)]
        modalHeadline = helpMessages[currentPanel].headline;
        modalText = helpMessages[currentPanel].message;
        $('#headline > strong').text(modalHeadline);
        $('#message').text(modalText);
    })

    $(document).on("click", '#ada_button', function(event) {
        if($(this).attr('data-active') == "true") {
            window.localStorage.setItem('cwt-12431_ada', 0);
            $(this).attr('data-active', 'false');
        } else {
            window.localStorage.setItem('cwt-12431_ada', 1);
            $(this).attr('data-active', 'true');
            $('#myOtherModal').modal('toggle');
        }
    });

    $(document).on("click", '#ada_button', function(event) {
        // toggle display
        if($('#ada_page_navigation').css('visibility') == 'visible') {
            // hide it
            $('#ada_page_navigation').css('visibility', 'hidden');

        } else {
            // show it
            $('#ada_page_navigation').css('visibility', 'visible');
        }
        if($('#page_navigation').css('visibility') == 'visible') {
            // hide it
            $('#page_navigation').css('visibility', 'hidden');
        } else {
            // show it
            $('#page_navigation').css('visibility', 'visible');
        }
    });



});

var panelHistory = [];

function initialize() {
    panelHistory = [];
    clock();
    showPanel('init');
    window.localStorage.setItem('cwt-12431_ada', 0);
    $('#ada_page_navigation').css('visibility', 'hidden');
    $('#page_navigation').css('visibility', 'visible');
    // load init tab
    ajaxGet('/ajax/tool/init', function(data) {
        replaceInnerHtml('init', data);
        addPanelHistoryItem('init');
    });
    // load session search tab
    ajaxGet('/ajax/tool/sessionSearch', function(data) {
        replaceInnerHtml('session_search', data);
    });
    window.localStorage.setItem('cwt-12431_ada', 0);
}

function reset() {

}

function ajaxGet(url, cb = null) {
    $.get(url, function(data, status) {
        if (typeof cb == "function") {
            cb(data);
        } else {
            // doNothing();
        }
    }).fail(function(error) {
        console.error(error);
    });
}

function replaceInnerHtml(targetElementId, content = "", cb = null) {
    $('#' + targetElementId).html(content);
    if(cb != null) {
        cb();
    }
}

function showPanel(panelId) {
    $('#' + panelId + '_tab').tab('show');
}

function clock() {
    var now = new Date();
    var TwentyFourHour = now.getHours();
    var hour = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    var mid = 'pm';
    if (min < 10) {
        min = "0" + min;
    }
    if (hour > 12) {
        hour = hour - 12;
    }
    if (hour == 0) {
        hour = 12;
    }
    if (TwentyFourHour < 12) {
        mid = 'am';
    }
    document.getElementById('clock').innerHTML = hour + ':' + min + '' + mid.toUpperCase();
    setTimeout(clock, 1000);
}

function addPanelHistoryItem(panelName) {
    panelHistory.push(panelName);
    // console.log(panelHistory)
}

function removePanelHistoryItem() {
    if (panelHistory.length > 1) {
        panelHistory.pop();
    }
}

// retrieves params from URL
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

var timeoutTime = 15000;
var timeoutTimer = setTimeout(ShowTimeOutWarning, timeoutTime);

var secondTimeoutTime = 30000;
var secondTimeoutTimer = setTimeout(resetThePage, secondTimeoutTime);

$(document).ready(function() {
    $('body').bind('mousedown keydown', function(event) {
        clearTimeout(timeoutTimer);
        timeoutTimer = setTimeout(ShowTimeOutWarning, timeoutTime);
        console.log('called end first timeout');
    });
});



function ShowTimeOutWarning() {
    $('#myOtherOtherModal').modal('toggle');

    console.log('called showtimeoutwarning');

}

$(document).ready(function() {
    $('body').bind('mousedown keydown', function(event) {
        clearTimeout(secondTimeoutTimer);
        secondTimeoutTimer = setTimeout(resetThePage, secondTimeoutTime);
        console.log('called end second timeout');
    });
});



function resetThePage() {
    $('#myOtherOtherModal').modal('toggle');
    console.log('called resetthtepage');
    initialize();
}
