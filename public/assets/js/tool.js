$(document).ready(function() {
    // A $( document ).ready() block.

    buttonsClicked = [];

    initialize();

    // $(document).on("keypress", '#session_search_field', function(event) {
    //
    //     if( $('#session_search_field').val().length >= 0 ) {
    //         console.log('test');
    //           $('#clear_button').css('visibility', 'visible');
    //     }
    // });


$(document).on("click", "button", function(event){
    console.log('clicked');
})




    $(document).on("click", "*", function() {
        startTimer();
    });


    $(document).on("click", 'button', function(event) {
        $('button').blur();
    });

    $(document).on("click", 'button', function(event) {
        if(typeof $(this).attr('data-buttonName') !== 'undefined') {
            console.log($(this).attr('data-buttonName'));
            buttonsClicked.push($(this).attr('data-buttonName'));
        }
    });

    $(document).on("click", '.loads-session_search', function(event) {
        event.preventDefault();
        addPanelHistoryItem('session_search');
        showPanel('session_search');
    });

    $('#routeStepsCarousel').carousel('pause');


    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-session_search_results', function(event) {
        event.preventDefault();
        if ($('#session_search_field').val().trim() == '') {
            // TODO reload search page w/error text
        } else {
            // console.log('/ajax/tool/sessionSearchResults/' + $('#session_search_field').val().trim().replace(' ', '%20'));

            // ajaxGet('/ajax/tool/sessionSearchResults/' + $('#session_search_field').val().trim().replace(' ', '%20'), function(data) {
            //     replaceInnerHtml('session_search_results', data, showPanel('session_search_results'));
            // });

            replaceInnerHtml('session_search_results', '<div class="w-100 h-100 row align-items-center justify-content-center"><div class="text-center d-flex"><div class="lds-dual-ring"></div></div></div>', showPanel('session_search_results'));
            ajaxGet('/ajax/tool/sessionSearchResults/' + $('#session_search_field').val().trim().replace(' ', '%20'), function(data) {
                replaceInnerHtml('session_search_results', data);
            });
            addPanelHistoryItem('session_search_results');
        }
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-rooms', function(event) {
        event.preventDefault();
        // console.log('/ajax/tool/rooms/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-venueId'));
        ajaxGet('/ajax/tool/rooms/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-venueId'), function(data) {
            replaceInnerHtml('rooms', data, showPanel('rooms'));
        });
        addPanelHistoryItem('rooms',$(this).attr('data-buttonName'));
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-venues', function(event) {
        event.preventDefault();
        ajaxGet('/ajax/tool/venues', function(data) {
            replaceInnerHtml('venues', data,
                showPanel('venues'));
        });
        addPanelHistoryItem('venues');
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.loads-venue-meals', function(event) {
        event.preventDefault();
        ajaxGet('/ajax/tool/venues', function(data) {
            replaceInnerHtml('venues', data,
                showPanel('venues'));
        });
        addPanelHistoryItem('venues');
    });

    // shows the session search results panel and fetches results for entered search terms
    $(document).on("click", '.venue-button', function(event) {
        event.preventDefault();
        ajaxGet('/ajax/tool/venues/' + $(this).attr('data-venueId') + '/' + getUrlParameter('startPointId'), function(data) {
            replaceInnerHtml('venue_menu', data,
                showPanel('venue_menu'));
        });
        addPanelHistoryItem('venue_menu',$(this).attr('data-buttonName'));
    });

    $(document).on("click", '.loads-pick-venues', function(event) {
        // console.log($(this).attr('data-venueId'));
        if ($(this).attr('data-venueId')) {
            var endVenueStr = '/' + $(this).attr('data-venueId');
            event.preventDefault();
            urlToCall = '/ajax/tool/pickVenues/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-endOption') + endVenueStr;
            // console.log(urlToCall.replace('',''));
            ajaxGet(urlToCall.replace(' ', '_'), function(data) {
                replaceInnerHtml('venue_menu', data,
                    showPanel('venue_menu'));
            });
            addPanelHistoryItem('venue_menu', $(this).attr('data-buttonName'));
        } else {
            // console.log('here');
            var endVenueStr = '';
            event.preventDefault();
            urlToCall = '/ajax/tool/pickVenues/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-endOption') + endVenueStr;
            // console.log(urlToCall);
            // console.log(urlToCall.replace('',''));
            ajaxGet(urlToCall.replace(' ', '_'), function(data) {
                replaceInnerHtml('venue_menu', data,
                    showPanel('venue_menu'));
            });
            addPanelHistoryItem('venue_menu', $(this).attr('data-buttonName'));
        }

    });

    $(document).on("click", '#back_button', function(event) {
        event.preventDefault();
        if (panelHistory.length > 1) {
            let reverseArray = panelHistory.slice();
            reverseArray = reverseArray.reverse();
            let targetPanel = reverseArray[1][0];
            showPanel(targetPanel);
            removePanelHistoryItem();
        }
    });

    // top_bar_search_button
    $(document).on("click", '#top_bar_search_button', function(event) {
        event.preventDefault();
        addPanelHistoryItem('init');
        showPanel('init');
        $('#collapseExample').collapse('show');
    });

    // top_bar_search_button
    $(document).on("click", '#ada_search_button', function(event) {
        event.preventDefault();
        addPanelHistoryItem('init');
        showPanel('init');
        $('#collapseExample').collapse('show');
    });

    $(document).on("click", '.back-button', function(event) {
        event.preventDefault();
        if (panelHistory.length > 1) {
            let reverseArray = panelHistory.slice();
            reverseArray = reverseArray.reverse();
            let targetPanel = reverseArray[1][0];
            showPanel(targetPanel);
            removePanelHistoryItem();
        }
    });

    $(document).on("click", '#ada_back_button', function(event) {
        event.preventDefault();
        if (panelHistory.length > 1) {
            let reverseArray = panelHistory.slice();
            reverseArray = reverseArray.reverse();
            let targetPanel = reverseArray[1][0];
            showPanel(targetPanel);
            removePanelHistoryItem();
        }
    });

    $(document).on("click", '.session-select-button', function(event) {
        event.preventDefault();
        this_elem = $(this);

        // console.log('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'));
        ajaxGet('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'), function(data) {
            if (data.trim().length == 0) {

                ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + 'walk' + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-venueId'), function(data) {
                    replaceInnerHtml('route_steps', data, showPanel('route_steps'));
                });
                addPanelHistoryItem('route_steps', $(this).attr('data-buttonName'));
            } else {
                replaceInnerHtml('transportation_options', data, showPanel('transportation_options'));
                addPanelHistoryItem('transportation_options', $(this).attr('data-buttonName'));

            }
        });
    });

    $(document).on("click", '.room-select-button', function(event) {
        event.preventDefault();
        this_elem = $(this);
        ajaxGet('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'), function(data) {
            if (data.trim().length == 0) {

                ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + 'walk' + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-venueId'), function(data) {
                    replaceInnerHtml('route_steps', data, showPanel('route_steps'));
                });
                addPanelHistoryItem('route_steps');
            } else {
                replaceInnerHtml('transportation_options', data, showPanel('transportation_options'));
                addPanelHistoryItem('transportation_options');
            }
        });
    });

    $(document).on("click", '.loads-route-steps', function(event) {
        event.preventDefault();
        this_elem = $(this);


        // console.log('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + $(this_elem).attr('data-transportationMode') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this_elem).attr('data-venueId'));


        ajaxGet('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-venueId'), function(data) {
            // console.log(data);
            if (data.trim().length == 0) {

                ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + 'walk' + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this_elem).attr('data-venueId'), function(data) {
                    // console.log(data);
                    replaceInnerHtml('route_steps', data,
                        showPanel('route_steps'));
                });
                addPanelHistoryItem('route_steps');
            } else {
                replaceInnerHtml('transportation_options', data);
                addPanelHistoryItem('transportation_options');
                showPanel('transportation_options');
            }
        });
    });


    $(document).on("click", '.transportation-option-select-button', function(event) {
        event.preventDefault();

        // console.log(($this));
        this_elem = $(this);
        ajaxGet('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/' + $(this).attr('data-transportationMode') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this).attr('data-venueId'), function(data) {

            // console.log('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this_elem).attr('data-endPointId') + '/' + $(this_elem).attr('data-transportationMode') + '/' + window.localStorage.getItem('cwt-12431_ada', 1) + '/' + $(this_elem).attr('data-venueId'));
            if (data.trim().length == 0) {

                initialize();
            } else {
                replaceInnerHtml('route_steps', data, showPanel('route_steps'));
                addPanelHistoryItem('route_steps');

            }


        });
    });

    $(document).on("click", '#home_button', function(event) {
        initialize();
        // make touchable area large and remove float to bottom
    });

    $(document).on("click", '#ada_home_button', function(event) {
        panelHistory = [];
        clock();
        showPanel('init');
        window.localStorage.setItem('cwt-12431_ada', 1);
        $('#ada_page_navigation').css('visibility', 'visible');
        $('#page_navigation').css('visibility', 'hidden');
        // load init tab
        ajaxGet('/ajax/tool/newHome/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada'), function(data) {
            replaceInnerHtml('init', data);
            addPanelHistoryItem('init');
            // make touchable area small and float to bottom'
            if(window.localStorage.getItem('cwt-12431_ada') == 1) {
                $('#init > div.touchable-area').removeClass('not-ada');
                $('#init > div.touchable-area').addClass('ada');
            } else {
                $('#init > div.touchable-area').addClass('not-ada');
                $('#init > div.touchable-area').removeClass('ada');
            }
        });
        // load session search tab
        ajaxGet('/ajax/tool/sessionSearch', function(data) {
            replaceInnerHtml('session_search', data);
        });
        window.localStorage.setItem('cwt-12431_ada', 1);
    });


    $(document).on("click", '#info_button', function(event) {

        $('#myModal').modal('toggle');
    });

    $(document).on("click", '#ada_info_button', function(event) {

        $('#myModal').modal('toggle');
    });

    $('#myModal').on('show.bs.modal', function(e) {
        currentPanel = panelHistory[(panelHistory.length - 1)][0];
        modalHeadline = helpMessages[currentPanel].headline;
        modalText = helpMessages[currentPanel].message;
        $('#headline > strong').text(modalHeadline);
        $('#message').text(modalText);
    })

    // $(document).on("click", '#ada_button', function(event) {
    //     if($(this).attr('data-active') == "true") {
    //         window.localStorage.setItem('cwt-12431_ada', 0);
    //         $(this).attr('data-active', 'false');
    //     } else {
    //         window.localStorage.setItem('cwt-12431_ada', 1);
    //         $(this).attr('data-active', 'true');
    //         $('#myOtherModal').modal('toggle');
    //     }
    // });

    $(document).on('change', '#customSwitch1', function() {
        if (this.checked) {
            // console.log('checked');
            window.localStorage.setItem('cwt-12431_ada', 1);
            $(this).attr('data-active', 'true');
            $('#myOtherModal').modal('toggle');
            // make touchable area smaller and float to bottom
            $('#init > div.touchable-area').removeClass('not-ada');
            $('#init > div.touchable-area').addClass('ada');
        } else {
            // console.log('unchecked');
            window.localStorage.setItem('cwt-12431_ada', 0);
            $(this).attr('data-active', 'false');
            // make touchable area large and remove float
            $('#init > div.touchable-area').removeClass('ada');
            $('#init > div.touchable-area').addClass('not-ada');
        }
    });

    // $(document).on("click", '#ada_button', function(event) {
    //     // toggle display
    //     if ($('#ada_page_navigation').css('visibility') == 'visible') {
    //         // hide it
    //         $('#ada_page_navigation').css('visibility', 'hidden');
    //
    //     } else {
    //         // show it
    //         $('#ada_page_navigation').css('visibility', 'visible');
    //     }
    //     if ($('#page_navigation').css('visibility') == 'visible') {
    //         // hide it
    //         $('#page_navigation').css('visibility', 'hidden');
    //     } else {
    //         // show it
    //         $('#page_navigation').css('visibility', 'visible');
    //     }
    // });

    $(document).on("change", '#customSwitch1', function(event) {
        console.log('footer: ADA switch');
        buttonsClicked.push('footer: ADA switch');
        // toggle display
        if ($('#ada_page_navigation').css('visibility') == 'visible') {
            // hide it
            $('#ada_page_navigation').css('visibility', 'hidden');

        } else {
            // show it
            $('#ada_page_navigation').css('visibility', 'visible');
        }
        if ($('#page_navigation').css('visibility') == 'visible') {
            // hide it
            $('#page_navigation').css('visibility', 'hidden');
        } else {
            // show it
            $('#page_navigation').css('visibility', 'visible');
        }
    });

    $(document).on("focusin", '#session_search_field', function(event) {
        // console.log('test');
        $('#collapseExample').collapse('show')
        // $('#whereDoYouWantToGoCollapse').collapse('hide');
    });

    $(document).on("focusout", '#session_search_field', function(event) {
        // console.log('test2');
        // $('#collapseExample').collapse('hide')
    });

    $(document).on("click", '#collapse_keyboard', function(event) {
        // console.log('test2');
        $('#collapseExample').collapse('hide')
        // $('#whereDoYouWantToGoCollapse').collapse('show');
    });




});

var panelHistory = [];



// var timeLeft = 10;
// var elem = document.getElementById('countdown_timer');

function initialize() {
    if(buttonsClicked.length > 0){
        // console.log(buttonsClicked);
        // ajaxPost("https://52.52.88.146/buttonClicks.cfm?startPointId="+getUrlParameter('startPointId'), {data: buttonsClicked.join(', ')});
    }

    if (panelHistory.length > 0) {
        // console.log(panelHistory);
    }
    panelHistory = [];
    clock();
    showPanel('init');
    // clearTimeout(timerId);
    // var timerId = setInterval(countdown, 1000);
    startTimer();
    window.localStorage.setItem('cwt-12431_ada', 0);
    $('#ada_page_navigation').css('visibility', 'hidden');
    $('#page_navigation').css('visibility', 'visible');
    // load init tab
    ajaxGet('/ajax/tool/newHome/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada'), function(data) {
        replaceInnerHtml('init', data);
        addPanelHistoryItem('init');
    });
    // load session search tab
    ajaxGet('/ajax/tool/sessionSearch', function(data) {
        replaceInnerHtml('session_search', data);
    });
    window.localStorage.setItem('cwt-12431_ada', 0);
    // console.log('/ajax/tool/init/' + getUrlParameter('startPointId') + '/' + window.localStorage.getItem('cwt-12431_ada'));
    // buttonsClicked = [];
}

function reset() {

}

function ajaxGet(url, cb = null) {
    $.get(url, function(data, status) {
        if (typeof cb == "function") {
            cb(data);
        } else {
            // doNothing();
        }
    }).fail(function(error) {
        console.error(error);
    });
}

function ajaxPost(url, formData, cb = null) {
    var posting = $.post( url, formData );

  // Put the results in a div
  posting.done(function( data ) {
    console.log(data);
});
posting.fail(function(err){
    console.log('error');
});
}

function replaceInnerHtml(targetElementId, content = "", cb = null) {
    $('#' + targetElementId).html(content);
    if (cb != null) {
        cb();
    }
}

function showPanel(panelId) {
    $('#' + panelId + '_tab').tab('show');
}

function clock() {
    var now = new Date();
    var TwentyFourHour = now.getHours();
    var hour = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    var mid = 'pm';
    if (min < 10) {
        min = "0" + min;
    }
    if (hour > 12) {
        hour = hour - 12;
    }
    if (hour == 0) {
        hour = 12;
    }
    if (TwentyFourHour < 12) {
        mid = 'am';
    }
    document.getElementById('clock').innerHTML = hour + ':' + min + '' + mid.toUpperCase();
    setTimeout(clock, 1000);
}

function addPanelHistoryItem(panelName, buttonClicked = null) {
    panelHistory.push([panelName]);
    // panelHistory[(panelHistory.length-1)][2] = buttonClicked;
    // console.log(panelHistory);
}

function removePanelHistoryItem() {
    if (panelHistory.length > 1) {
        panelHistory.pop();
    }
}

function getCurrentPanel() {
    // console.log(panelHistory[(panelHistory.length - 1)][0]);
    return panelHistory[(panelHistory.length - 1)][0];
}

// retrieves params from URL
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

// var timeoutTime = 30000;
// var timeoutTimer = setTimeout(ShowTimeOutWarning, timeoutTime);
//
// var secondTimeoutTime = 45000;
// var secondTimeoutTimer = setTimeout(resetThePage, secondTimeoutTime);




// function ShowTimeOutWarning() {
//     console.log(getCurrentPanel());
//     if(getCurrentPanel() != 'init') {
//         $('#myOtherOtherModal').on('show.bs.modal', function (e) {
//           $('.modal').modal('hide')
//       });
//         $('#myOtherOtherModal').modal('toggle');
//
//         // console.log('called showtimeoutwarning');
//     }
//
//
// }

$(function() {
    $('#myModal').on('show.bs.modal', function() {
        var myModal = $(this);
        clearTimeout(myModal.data('hideInterval'));
        myModal.data('hideInterval', setTimeout(function() {
            myModal.modal('hide');
        }, 20000));
    });
});



// function resetThePage() {
//     if (getCurrentPanel() != 'init') {
//         $('#myOtherOtherModal').modal('toggle');
//         // console.log('called resetthtepage');
//         initialize();
//     } else {
//         $('#myOtherOtherModal').modal('toggle');
//         clearTimeout(timerId);
//         var timeLeft = 60;
//         var timerId = setInterval(countdown, 1000);
//     }
// }
//
// var timeLeft = 60;
// var elem = document.getElementById('countdown_timer');
//


// function countdown() {
//     console.log(timeLeft);
//     if (timeLeft == 5 && getCurrentPanel() != 'init') {
//         // $('#myOtherOtherModal').modal('show');
//         console.log('WARNING');
//     } else {
//         // doNothing();
//     }
//
//     if (timeLeft == 0) {
//         if(getCurrentPanel() != 'init') {
//             console.log('SCREEN RESET')
//         } else {
//             console.log('RESET TIMER');
//         }
//     } else {
//         elem.innerHTML = timeLeft;
//         timeLeft--;
//     }
// }

var timeLeft;
var elem = document.getElementById('countdown_timer');
var timerId;

function countdown() {
    // console.log(timeLeft);
    if (timeLeft == 0) {
        // doSomething();
        console.log(getCurrentPanel());
        if (getCurrentPanel() != 'init') {
            $('#myOtherOtherModal').modal('hide');
            initialize();
        } else {
            startTimer();
        }
    } else {
        elem.innerHTML = timeLeft;
        timeLeft--;
        if (timeLeft == 29 && getCurrentPanel() != 'init') {
            $('.modal').modal('hide')
            $('#myOtherOtherModal').modal('show');
        } else {
            // doNothing();
        }
    }
}

function startTimer() {
    clearInterval(timerId);
    timeLeft = 60;
    timerId = setInterval(countdown, 1000);
}
