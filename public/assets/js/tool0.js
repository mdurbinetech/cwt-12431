console.log('The hatches are open!');

// A $( document ).ready() block.
$(document).ready(function() {

    // destroy userSession
    destroyUserSession();

    // create user session
    createUserSession();

    // load init page
    getAndPlaceHtml('/ajax/tool/init', '#init')

    // TODO write function that calls an ajaz page to record element clicks
    // Also create a spreadsheet for Kevin to include in database with
    // description of what is clicked and its CSS selector

    $(document).on("click", '.loads-venues', function(event) {
        if ($('#venues').text().trim().length == 0) {
            getAndPlaceHtml('/ajax/tool/venues', '#venues');
        } else {
            // doNothing();
        }
    });

    $(document).on("click", '.loads-venue-menu', function(event) {
        if ($('#venues').text().trim().length == 0) {
            getAndPlaceHtml('/ajax/tool/venues/' + $(this).attr('data-venueId'), '#venueMenu');
        } else {
            getAndPlaceHtml('/ajax/tool/venues/' + $(this).attr('data-venueId'), '#venueMenu');
        }
    });

    $(document).on("click", '.loads-panel', function(event) {
        event.preventDefault();
        let targetPanel = $(this).attr('data-targetPanel');
        switchPanel(targetPanel);
    });

    $(document).on("click", '.loads-sessionSearch', function(event) {
        if ($('#sessionSearch').text().trim().length == 0) {
            getAndPlaceHtml('/ajax/tool/sessionSearch', '#sessionSearch');
        } else {
            getAndPlaceHtml('/ajax/tool/sessionSearch', '#sessionSearch');
        }
    });

    $(document).on("click", '.loads-sessionSearchResults', function(event) {
        if($('#sessionSearchField').val() == "") {
            alert('Please enter a search term');
            switchPanel('sessionSearch');
        } else {
            if ($('#sessionSearchResults').text().trim().length == 0) {
                getAndPlaceHtml('/ajax/tool/sessionSearchResults/' + $('#sessionSearchField').val(), '#sessionSearchResults');
            } else {
                getAndPlaceHtml('/ajax/tool/sessionSearchResults/' + $('#sessionSearchField').val(), '#sessionSearchResults');
            }
        }
    });

    $(document).on("click", '.clicks-tracked', function(event) {
        let clickTrackName = $(this).attr('data-clickTrackName');
        addClickHistoryItem(clickTrackName, event.clientX, event.clientY);
    });

    $(document).on("click", '.loads-transportationOptions', function(event) {
        if ($('#venues').text().trim().length == 0) {
            getAndPlaceHtml('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endVenueId'), '#transportationOptions');
        } else {
            getAndPlaceHtml('/ajax/tool/transportationOptions/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endVenueId'), '#transportationOptions');
        }
    });

    $(document).on("click", '.loads-routeSteps', function(event) {
        if ($('#venues').text().trim().length == 0) {
            getAndPlaceHtml('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/walk/0', '#routeSteps');
        } else {
            getAndPlaceHtml('/ajax/tool/routeSteps/' + getUrlParameter('startPointId') + '/' + $(this).attr('data-endPointId') + '/walk/0', '#routeSteps');
        }
    });

    $(document).on("click", '#backButton', function(event) {
        if ($(this).attr('data-targetPanel') == '') {
            // doNothing();
        } else {
            switchPanel($(this).attr('data-targetPanel'));
        }
    });

});

// function wrapper for getting and placing HTML content
function getAndPlaceHtml(url, targetElemSelector) {
    $.get(url, function(data, status) {
        $(targetElemSelector).html(data);
    });
}

// switch panel and save history information to a temporary var
function switchPanel(targetPanel) {
    addPanelHistoryItem(targetPanel);
    $('#' + targetPanel + '-tab').tab('show');
}

// clears history of panels visited in current session
function clearPanelHistory() {
    // TODO -- probably will never need this
}

// adds item to panel history
function addPanelHistoryItem(currentPanelName) {
    let userSession = JSON.parse(sessionStorage.userSession_cwt_12431);
    let panelHistoryItem = {
        panelOpenTime: Date.now(),
        panelName: currentPanelName,
        panelVars: [9,9,9,9,9]
    };
    userSession.panelHistory.push(panelHistoryItem);
    sessionStorage.setItem('userSession_cwt_12431', JSON.stringify(userSession));
    backwardsHistory = userSession.panelHistory.reverse();
    setBackButtonTargetPanel(backwardsHistory[1].panelName);
}

// adds item to click history
function addClickHistoryItem(clickedItemName, mouseX, mouseY) {
    let userSession = JSON.parse(sessionStorage.userSession_cwt_12431);
    let clickHistoryItem = {
        clickTime: Date.now(),
        clickedItemName: clickedItemName,
        clickX: mouseX,
        clickY: mouseY
    };
    userSession.clickHistory.push(clickHistoryItem);
    sessionStorage.setItem('userSession_cwt_12431', JSON.stringify(userSession));
}

// creates a "session" for tracking user journey through tool
function createUserSession() {
    let userSession = {
        startTime: Date.now(),
        panelHistory: [{
            panelOpenTime: Date.now(),
            panelName: "init"
        }],
        clickHistory: []
    };
    sessionStorage.setItem('userSession_cwt_12431', JSON.stringify(userSession));
}

function getUserSession() {
    return JSON.parse(sessionStorage.userSession_cwt_12431);
}

// destroys a user's "session"
function destroyUserSession() {
    sessionStorage.removeItem('userSession_cwt_12431');
}

// inserts a database record with user session information
function saveUserSession() {
    // TODO ajax call to save locally stored var in the database via PHP page
}

// resets application back to initial state, clears panel history, begins
// a new user session
function reset() {

}

// sets the target of the back button
function setBackButtonTargetPanel(panelName) {
    $('#backButton').attr('data-targetPanel', panelName);
}

// sets the current panel
function setCurrentPanel(panelName) {
    $('#tabNavContent').attr('data-currentPanel', panelName);
}

// retrieves params from URL
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
