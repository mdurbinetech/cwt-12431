# cwt-12431

1. Clone repo into web server public root
    ```
    git clone https://mdurbinetech@bitbucket.org/mdurbinetech/cwt-12431.git
    ```

2. Copy .htaccess from cwt-12431 into server public root
    ```
    cd cwt-12431
    cp .htaccess ../.htaccess
    ```

3. Install node.js dependencies
    ```
    cd resources
    npm install --save
    ```

4. Navigate to your server public root URL (ex: 127.0.0.1) to view the index page
